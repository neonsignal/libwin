experimental GUI interface classes, as an alternative to MFC in a Win32 context, running standalone or embedded as a NPAPI browser plugin

Copyright 2002 Glenn McIntosh

licensed under the GNU General Public License version 3, see [LICENSE.md](LICENSE.md)
