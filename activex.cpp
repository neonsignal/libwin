// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include "types.h"
	#include "libwin.h"
// ActiveX
	ActiveX::ActiveX()
	{
	// allow access to OLE
		::CoInitialize(0);
	}
	ActiveX::~ActiveX()
	{
	// give up access to OLE
		::CoUninitialize();
	}
// COM objects and interfaces
	ComObject::ComObject(CLSID const &classId) : mIUnknown(0)
	{
		::CoCreateInstance(classId, 0, CLSCTX_SERVER, IID_IUnknown, (void **) &mIUnknown);
	}
	ComObject::~ComObject()
	{
		if (mIUnknown)
			mIUnknown->Release();
	}
	void *ComObject::acquire(IID const &iid)
	{
		void *pInterface = 0;
		mIUnknown->QueryInterface(iid, &pInterface);
		return pInterface;
	}
