// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _ACTIVEX_H
#define _ACTIVEX_H
/** ActiveX.
	This singleton provides global access to the ActiveX object system.
	*/
	class ActiveX: public Singleton<ActiveX>
	{
	public:
		ActiveX();
		~ActiveX();
	};
/** COM Interface Access.
	*/
	class ComObject
	{
	public:
		ComObject(CLSID const &classId);
		~ComObject();
		void *acquire(IID const &iid);
	private:
		IUnknown *mIUnknown;
	};
	template<class Interface, IID const *iid> class ComInterface
	{
	public:
		ComInterface(ComObject &obj) : mPInterface(static_cast<Interface *>(obj.acquire(*iid))) {}
		~ComInterface() {if (mPInterface) mPInterface->Release();}
	public:
		Interface* operator->() {return mPInterface;}
	private:
		Interface *mPInterface;
	};
/** COM Parameter.
	*/
	typedef VARIANT *PVARIANT;
	class ComParam
	{
	public:
		ComParam(bool b) {mVariant.vt = VT_BOOL; mVariant.bVal = b;}
		ComParam(int32_t i) {mVariant.vt = VT_I4; mVariant.lVal = i;}
		~ComParam() {}
	public:
		operator PVARIANT() {return &mVariant;}
	private:
		VARIANT mVariant;
	};
/** COM boolean.
	*/
	class ComBool
	{
	public:
		ComBool(bool b) : mB(b) {}
		~ComBool() {}
	public:
		operator VARIANT_BOOL() {return mB;}
	private:
		VARIANT_BOOL mB;
	};
/** COM string.
	*/
	class ComStr
	{
	public:
		ComStr(const wchar_t *str) : mStr(::SysAllocString(str)) {}
		~ComStr() {::SysFreeString(mStr);}
	public:
		operator BSTR() {return mStr;}
	private:
		BSTR mStr;
	};
#endif
