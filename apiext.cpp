// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include "types.h"
	#include "libwin.h"
// menu functions
	bool enableMenuCommand(HWND hwnd, int idCommand, bool enable)
	{
		MENUITEMINFO menuItemInfo = {sizeof(MENUITEMINFO), MIIM_STATE, 0, enable ? MFS_ENABLED : MFS_GRAYED, 0, NULL, NULL, NULL, 0, 0, 0};
		::SetMenuItemInfo(::GetMenu(hwnd), idCommand, FALSE, &menuItemInfo);
	}
// message loop
	int messageLoop(MenuFrame &menuFrame, int idAccelerators)
	{
		HACCEL haccel = ::LoadAccelerators(Resources::instance().handle(), MAKEINTRESOURCE(idAccelerators));
		MSG message;
		int status;
		while ((status = ::GetMessage(&message, 0, 0, 0)) > 0)
			if (!::TranslateAccelerator(menuFrame, haccel, &message) && !::IsDialogMessage(menuFrame, &message))
			{
				::TranslateMessage(&message);
				// TODO use resource lock to control access to API
				UiAccess uiAccess;
				::DispatchMessage(&message);
			}
		if (status == 0)
			status = message.wParam;
		return status;
	}
