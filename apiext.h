// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file apiext.h API function wrappers.
	*/
#ifndef _APIEXT_H
#define _APIEXT_H
// window pointers
	template <class T> inline T WinGetLong (HWND hwnd, int which = GWL_USERDATA)
	{
		return reinterpret_cast<T>(::GetWindowLong (hwnd, which));
	}
	template <class T> inline T WinSetLong (HWND hwnd, T value, int which = GWL_USERDATA)
	{
		return reinterpret_cast<T>(::SetWindowLong(hwnd, which, reinterpret_cast<long>(value)));
	}
// menu functions
	bool enableMenuCommand(HWND hwnd, int idCommand, bool enable);
// message loop
	int messageLoop(MenuFrame &menuFrame, int idAccelerators);
// thread class
	template<class User> class Thread
	{
	public:
		Thread(User *pUser, void (User::*pFn)()) : mPUser(pUser), mPFn(pFn) {mHandle = ::CreateThread(0, 0, reinterpret_cast<DWORD (WINAPI *)(void *)>(threadFn), this, 0, &mThreadId);}
		~Thread() {::WaitForSingleObject(mHandle, INFINITE); ::CloseHandle(mHandle);}
	private:
		User *mPUser;
		void (User::*mPFn)();
		static DWORD WINAPI threadFn(Thread<User> *pThis) {(pThis->mPUser->*pThis->mPFn)();}
		HANDLE mHandle;
		DWORD mThreadId;
	};
#endif
