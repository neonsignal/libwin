// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include <sstream>
	#include <fstream>
	#include "types.h"
	#include "libwin.h"
// chart line
	void ChartLine::setXZoom(real xOffset, real xSize)
	{
		mXOffset = xOffset; mXSize = xSize;
	}
	void ChartLine::setYZoom(real yOffset, real ySize)
	{
		mYOffset = yOffset; mYSize = ySize;
	}
	void ChartLine::xRange(real &xMin, real &xMax)
	{
		xMin = 0.; xMax = 1.;
		if (mX.size() > 0)
			xMin = xMax = mX[0];
		for (uint i = 1; i < mX.size(); ++i)
		{
			xMin = std::min(xMin, mX[i]);
			xMax = std::max(xMax, mX[i]);
		}
	}
	void ChartLine::yRange(real &yMin, real &yMax)
	{
		yMin = 0.; yMax = 1.;
		if (mX.size() > 0)
			yMin = yMax = mY[0];
		for (uint i = 1; i < mX.size(); ++i)
		{
			yMin = std::min(yMin, mY[i]);
			yMax = std::max(yMax, mY[i]);
		}
	}
	bool ChartLine::clipLeft(real &x0, real &y0, real &x1, real &y1, bool &clip1)
	{
	// check if inside line
		if (x0 < 0. && x1 < 0.)
		{
			clip1 = true;
			return false;
		}
	// clip x0 on 0. line
		if (x0 < 0. && x1 >= 0.)
		{
			y0 = y1 - (x1-0.)/(x1-x0)*(y1-y0);
			x0 = 0.;
		}
	// or clip x1 on 0. line
		else if (x0 >= 0. && x1 < 0.)
		{
			y1 = y0 - (x0-0.)/(x0-x1)*(y0-y1);
			x1 = 0.;
			clip1 = true;
		}
		return true;
	}
	bool ChartLine::clipRight(real &x0, real &y0, real &x1, real &y1, bool &clip1)
	{
	// check if inside line
		if (x0 >= 1. && x1 >= 1.)
		{
			clip1 = true;
			return false;
		}
	// clip x0 on 1. line
		if (x0 >= 1. && x1 < 1.)
		{
			y0 = y1 - (x1-1.)/(x1-x0)*(y1-y0);
			x0 = 1.;
		}
	// or clip x1 on 1. line
		else if (x0 < 1. && x1 >= 1.)
		{
			y1 = y0 - (x0-1.)/(x0-x1)*(y0-y1);
			x1 = 1.;
			clip1 = true;
		}
		return true;
	}
	void ChartLine::paint()
	{
		real x0 = 0., y0 = 0.;
		bool clip0 = false;
		int size = mX.size();
		for (int i = 0; i < size; ++i)
		{
		// rescale
			real xScale1 = (mX[i]-mXOffset)/mXSize, yScale1 = (mY[i]-mYOffset)/mYSize;
		// clip
			real x1 = xScale1, y1 = yScale1;
			bool clip1 = false;
			if (clipLeft(x0, y0, x1, y1, clip1) && clipRight(x0, y0, x1, y1, clip1) && clipLeft(y0, x0, y1, x1, clip1) && clipRight(y0, x0, y1, x1, clip1))
			{
				if (clip0)
					mPen.add(x0, y0);
				mPen.add(x1, y1);
				if (clip1)
					mPen.close();
			}
		// save new point
			x0 = xScale1; y0 = yScale1;
			clip0 = clip1;
		}
		if (!clip0)
			mPen.close();
		mPen.draw();
	}
// chart axes
	// base chart axis
		int AxisLine::division0(real tick)
		{
			return static_cast<int>(ceil(tick/(pow(10., mScale)*mScaleFactor)));
		}
		int AxisLine::division1(real tick)
		{
			return static_cast<int>(floor(tick/(pow(10., mScale)*mScaleFactor)));
		}
		real AxisLine::tick(int division, int exponent)
		{
			return division*(pow(10., mScale-exponent)*mScaleFactor);
		}
		void AxisLine::setZoom(real offset, real size)
		{
			mOffset = offset;
			mSize = size;
		}
		void AxisLine::reduceScale()
		{
			if (mScaleFactor == 1)
				--mScale;
			const int mPNextScale[] = {0, 5, 1, 0, 0, 2};
			mScaleFactor = mPNextScale[mScaleFactor];
		}
		void AxisLine::paint()
		{
		// generate axis
			generateAxis();
		// calculate maximum tick interval
			const int minTicks = 4;
			mScale = static_cast<int>(ceil(log10(mSize/minTicks)));
			mScaleFactor = 1;
		// rescale until required number of ticks
			while ((division1(mOffset+mSize) - division0(mOffset) + 1) < minTicks)
				reduceScale();
		// calculate tick label exponent
			int maxExponent = static_cast<int>(floor(log10(std::max(std::abs(mOffset), std::abs(mOffset+mSize)))));
			const int exponentInterval = 3;
			int exponent = (maxExponent + (maxExponent >= 0 ? 1 : 2-exponentInterval)) /exponentInterval * exponentInterval;
			if (exponent == 3) exponent = 0;
		// calculate tick label decimal places
			const int minPrecision = 3;
			int decimalPlaces = std::max(1+maxExponent-mScale, minPrecision) - (1+maxExponent-exponent);
			if (decimalPlaces < 0) decimalPlaces = 0;
		// generate ticks
			int tick0 = division0(mOffset), tick1 = division1(mOffset+mSize);
			for (int iTick = tick0; iTick <= tick1; ++iTick)
			{
			// draw tick
				generateTick((tick(iTick)-mOffset)/mSize, 0.02);
			// set tick label format
				real tickValue = tick(iTick, exponent);
				std::ostringstream tickLabel;
				tickLabel.setf(std::ios::fixed);
			// generate mantissa
				tickLabel.precision(decimalPlaces);
				tickLabel << tickValue;
			// write tick label
				generateTick(tickLabel.str(), (tick(iTick)-mOffset)/mSize);
			}
		// generate exponent
			int midTick = tick0+tick1-1;
			midTick = (midTick < 0 ? midTick-1 : midTick) / 2;
			if (exponent != 0)
			{
				std::ostringstream exponentLabel;
				exponentLabel << "x10e" << exponent;
				generateTick(exponentLabel.str(), ((tick(midTick)+tick(midTick+1))/2-mOffset)/mSize);
			}
		// generate halfticks
			reduceScale();
			reduceScale();
			tick0 = division0(mOffset); tick1 = division1(mOffset+mSize);
			for (int iTick = tick0; iTick <= tick1; ++iTick)
				generateTick((tick(iTick)-mOffset)/mSize, 0.01);
		// draw
			mPen.draw();
		}
	// X axis
		void XAxisLine::generateAxis()
		{
			mPen.add(0., 0.);
			mPen.add(1., 0.);
			mPen.close();
			mPen.add(0., 1.);
			mPen.add(1., 1.);
			mPen.close();
		}
		void XAxisLine::generateTick(real x, real size)
		{
			mPen.add(x, 0.);
			mPen.add(x, size);
			mPen.close();
		}
		void XAxisLine::generateTick(const Str &label, real x)
		{
			int size = mTickFont.size();
			mTickFont.setAlign(FixedFont::Centre, FixedFont::Top);
			mTickFont.write(label, x, 0., 0, size/4);
		}
	// Y axis
		void YAxisLine::generateAxis()
		{
			mPen.add(0., 0.);
			mPen.add(0., 1.);
			mPen.close();
			mPen.add(1., 0.);
			mPen.add(1., 1.);
			mPen.close();
		}
		void YAxisLine::generateTick(real x, real size)
		{
			mPen.add(0., x);
			mPen.add(size, x);
			mPen.close();
			mPen.add(1., x);
			mPen.add(1.-size, x);
			mPen.close();
		}
		void YAxisLine::generateTick(const Str &label, real x)
		{
			int size = mTickFont.size();
			mTickFont.setAlign(FixedFont::Right, FixedFont::Middle);
			mTickFont.write(label, 0., x, -size/4, 0);
			mTickFont.setAlign(FixedFont::Left, FixedFont::Middle);
			mTickFont.write(label, 1., x, size/4, 0);
		}
// chart pane
	ChartPane::ChartPane()
	: TemplatePane(),
		mFontSize(12), mChartPen(*this, RGB(0xFF,0x00,0x00)), mAxisPen(*this), mCursor(*this), mTickFont(*this, mFontSize, "Roman"),
		mZoom(false),
		mChartLine(mChartPen), mXAxisLine(mAxisPen, mTickFont), mYAxisLine(mAxisPen, mTickFont),
		mXZoomMode(ZoomMax), mYZoomMode(ZoomMax), mXZoomed(false), mYZoomed(false)
	{
	// initialize chart
		setZoomScroll(60.);
	}
	ChartPane::~ChartPane()
	{
	}
	void ChartPane::key(int key)
	{
		switch (key)
		{
		case VK_HOME|0x100: zoomMax(); break;
		case VK_END|0x100: zoomScroll(); break;
		case VK_PRIOR|0x100: case '-': zoomOut(); break;
		case VK_NEXT|0x100: case '+': zoomIn(); break;
		case VK_LEFT|0x100: zoomLeft(); break;
		case VK_RIGHT|0x100: zoomRight(); break;
		case VK_UP|0x100: zoomUp(); break;
		case VK_DOWN|0x100: zoomDown(); break;
		}
		update();
	}
	void ChartPane::zoomScroll()
	{
		setZoomScroll(60.);
		update();
	}
	void ChartPane::zoomMax()
	{
		setZoomMax();
		update();
	}
	void ChartPane::zoomOut()
	{
		real scale = 1.4142;
		mXOffset -= mXSize*(scale-1)/2;
		mYOffset -= mYSize*(scale-1)/2;
		mXSize *= scale;
		mYSize *= scale;
		mXZoomed = true; setXZoom();
		mYZoomed = true; setYZoom();
		update();
	}
	void ChartPane::zoomIn()
	{
		real scale = 1.4142;
		mXSize /= scale;
		mYSize /= scale;
		mXOffset += mXSize*(scale-1)/2;
		mYOffset += mYSize*(scale-1)/2;
		mXZoomed = true; setXZoom();
		mYZoomed = true; setYZoom();
		update();
	}
	void ChartPane::zoomLeft()
	{
		mXOffset -= mXSize/5;
		mXZoomed = true; setXZoom();
		update();
	}
	void ChartPane::zoomRight()
	{
		mXOffset += mXSize/5;
		mXZoomed = true; setXZoom();
		update();
	}
	void ChartPane::zoomUp()
	{
		mYOffset += mYSize/5;
		mYZoomed = true; setYZoom();
		update();
	}
	void ChartPane::zoomDown()
	{
		mYOffset -= mYSize/5;
		mYZoomed = true; setYZoom();
		update();
	}
	void ChartPane::pointer(int x, int y, bool button, bool down)
	{
		if (button && down)
		{
			mZoom = true;
			// convert to relative coordinates
				mX0 = mChartPen.xPos(x);
				mY0 = mChartPen.yPos(y);
		}
		if (mZoom)
		{
			// convert to relative coordinates
				real x1, y1;
				x1 = mChartPen.xPos(x);
				y1 = mChartPen.yPos(y);
			// draw cursor
				mCursor.draw(mX0, mY0, x1, y1);
		}
		if (button && !down)
		{
			mZoom = false;
			// clear cursor
				mCursor.close();
			// convert to relative coordinates
				real x1, y1;
				x1 = mChartPen.xPos(x);
				y1 = mChartPen.yPos(y);
			// update x
				if (x1 < mX0) std::swap(x1, mX0);
				if (x1-mX0 > 0.03)
				{
					if (x1 < mX0) std::swap(x1, mX0);
					mXOffset += mX0*mXSize;
					mXSize = (x1-mX0)*mXSize;
					mXZoomed = true; setXZoom();
				}
				else
					mXZoomed = false;
			// update y
				if (y1 < mY0) std::swap(y1, mY0);
				if (y1-mY0 > 0.03)
				{
					mYOffset += mY0*mYSize;
					mYSize = (y1-mY0)*mYSize;
					mYZoomed = true; setYZoom();
				}
				else
					mYZoomed = false;
			update();
		}
	}
	void ChartPane::setRegion(int left, int top, int right, int bottom)
	{
		int yWidth = mYAxisLine.width();
		int xWidth = mXAxisLine.width();
		mChartPen.setRegion(left+yWidth,top+xWidth/4, right-yWidth,bottom-xWidth/2);
		mAxisPen.setRegion(left+yWidth,top+xWidth/4, right-yWidth,bottom-xWidth/2);
		mCursor.setRegion(left+yWidth,top+xWidth/4, right-yWidth,bottom-xWidth/2);
		mTickFont.setRegion(left+yWidth,top+xWidth/4, right-yWidth,bottom-xWidth/2);
	}
	void ChartPane::setXZoom()
	{
	// set default zoom
		mXSize = std::max(mXSize, (std::abs(mXOffset)+1e-8)/1e8);
		mXAxisLine.setZoom(mXOffset, mXSize);
		mChartLine.setXZoom(mXOffset, mXSize);
	}
	void ChartPane::setYZoom()
	{
	// set default zoom
		mYSize = std::max(mYSize, (std::abs(mYOffset)+1e-8)/1e8);
		mYAxisLine.setZoom(mYOffset, mYSize);
		mChartLine.setYZoom(mYOffset, mYSize);
	}
	void ChartPane::paint()
	{
	// get area
		RECT size;
		::GetClientRect(*this, &size);
	// display line
		setRegion(0,0, size.right,size.bottom);
	// find x range
		if (!mXZoomed)
		{
		// find range
			real xMin, xMax;
			mChartLine.xRange(xMin, xMax);
		// change zoom
			switch (mXZoomMode)
			{
			case ZoomMax: setXZoom(xMin, xMax); break;
			case ZoomScrollBegin: setXZoom(xMin, xMin+mXZoomSize); break;
			case ZoomScrollEnd: setXZoom(xMax-mXZoomSize, xMax); break;
			}
		}
	// find y range
		if (!mYZoomed)
		{
		// find range
			real yMin, yMax;
			mChartLine.yRange(yMin, yMax);
		// change zoom
			switch (mYZoomMode)
			{
			case ZoomMax: setYZoom(yMin, yMax); break;
			case ZoomScrollBegin: setYZoom(yMin, yMin+mYZoomSize); break;
			case ZoomScrollEnd: setYZoom(yMax-mYZoomSize, yMax); break;
			}
		}
	// repaint axes
		mXAxisLine.paint();
		mYAxisLine.paint();
	// repaint data lines
		mChartLine.paint();
	// redraw cursor
		mCursor.draw();
	}
	void ChartPane::setXZoom(real xOffset0, real xOffset1)
	{
		mXOffset = xOffset0; mXSize = xOffset1-xOffset0;
		setXZoom();
	}
	void ChartPane::setYZoom(real yOffset0, real yOffset1)
	{
		mYOffset = yOffset0; mYSize = yOffset1-yOffset0;
		setYZoom();
	}
	void ChartPane::setZoomMax()
	{
	// change zoom mode
		mXZoomMode = ZoomMax; mYZoomMode= ZoomMax;
		mXZoomed = false; mYZoomed = false;
	// defer calculation to paint
	}
	void ChartPane::setZoomScroll(real xWidth)
	{
	// change zoom mode
		mXZoomSize = xWidth;
		mXZoomMode = ZoomScrollEnd; mYZoomMode = ZoomMax;
		mXZoomed = false; mYZoomed = false;
	// defer calculation to paint
	}
