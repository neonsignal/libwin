// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _CHART_H
#define _CHART_H
// chart line
	class ChartLine
	{
	public:
		ChartLine(LinePen &pen) : mPen(pen) {}
		~ChartLine() {}
		void setXZoom(real xOffset, real xSize), setYZoom(real yOffset, real ySize);
		std::vector<real> &x() {return mX;}
		std::vector<real> &y() {return mY;}
		void xRange(real &xMin, real &xMax), yRange(real &yMin, real &yMax);
		void paint();
	private:
		bool clipLeft(real &x0, real &y0, real &x1, real &y1, bool &clip1);
		bool clipRight(real &x0, real &y0, real &x1, real &y1, bool &clip1);
	private:
		LinePen &mPen;
		real mXOffset, mYOffset;
		real mXSize, mYSize;
		uint mI;
		std::vector<real> mX;
		std::vector<real> mY;
	};
// chart axes
	// base chart axis
		class AxisLine
		{
		public:
			AxisLine(LinePen &pen, FixedFont &tickFont) : mPen(pen), mTickFont(tickFont) {}
			~AxisLine() {}
			void setZoom(real offset, real size);
			void paint();
			virtual int width() = 0;
		private:
			int division0(real tick);
			int division1(real tick);
			real tick(int division, int exponent = 0);
			void reduceScale();
			virtual void generateAxis() = 0;
			virtual void generateTick(real x, real y) = 0;
			virtual void generateTick(const Str &label, real x) = 0;
		protected:
			LinePen &mPen;
			FixedFont &mTickFont;
		private:
			int mScale, mScaleFactor;
			real mOffset, mSize;
		};
	// X axis
		class XAxisLine: public AxisLine
		{
		public:
			XAxisLine(LinePen &pen, FixedFont &tickFont) : AxisLine(pen, tickFont) {}
			~XAxisLine() {}
			virtual int width() {return mTickFont.size()*3;}
		private:
			virtual void generateAxis();
			virtual void generateTick(real x, real y);
			virtual void generateTick(const Str &label, real x);
		};
	// Y axis
		class YAxisLine: public AxisLine
		{
		public:
			YAxisLine(LinePen &pen, FixedFont &tickFont) : AxisLine(pen, tickFont) {}
			~YAxisLine() {}
			virtual int width() {return mTickFont.size()*3;}
		private:
			virtual void generateAxis();
			virtual void generateTick(real x, real y);
			virtual void generateTick(const Str &label, real x);
		};
// chart pane
	class ChartPane: public TemplatePane
	{
	public:
		ChartPane();
		virtual ~ChartPane();
		void zoomOut(), zoomIn(), zoomLeft(), zoomRight(), zoomUp(), zoomDown();
		void zoomMax(), zoomScroll();
	public:
		std::vector<real> &x() {return mChartLine.x();}
		std::vector<real> &y() {return mChartLine.y();}
		void redraw() {update();}
	private:
		virtual void paint();
		virtual void key(int key);
		virtual void pointer(int x, int y, bool button, bool down);
	private:
		void setRegion(int left, int top, int right, int bottom);
		void zoomBegin(int x, int y), zoomMove(int x, int y), zoomEnd(int x, int y);
		void setXZoom(real xOffset0, real xOffset1), setYZoom(real yOffset0, real yOffset1);
		void setZoomMax(), setZoomScroll(real xWidth);
		void setXZoom(), setYZoom();
	private:
		ChartLine mChartLine;
		YAxisLine mYAxisLine;
		XAxisLine mXAxisLine;
	private:
		real mXOffset, mYOffset;
		real mXSize, mYSize;
		real mX0, mY0;
		bool mZoom;
		enum {ZoomMax, ZoomScrollBegin, ZoomScrollEnd} mXZoomMode, mYZoomMode;
		bool mXZoomed, mYZoomed;
		real mXZoomSize, mYZoomSize;
	private:
		int mFontSize;
		LinePen mChartPen, mAxisPen;
		CursorPen mCursor;
		ProportionalFont mTickFont;
	};
#endif
