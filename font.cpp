// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include "types.h"
	#include "libwin.h"
// font handle information
	Font::Font(Ui &window, uint size, const char *name, bool bold)
	: mWindow(window)
	{
		assert(mHfont = ::CreateFont(size, 0, 0, 0, bold ? FW_BOLD : FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, name));
	}
	Font::~Font()
	{
		::DeleteObject(mHfont);
	}
	void Font::select()
	{
		::SelectObject(mWindow, mHfont);
	}
// fixed font
	FixedFont::FixedFont(Ui &window, uint size, const char *name, bool bold)
	: Font(window, size, name, bold),
		mSize(size), mXAlign(Left), mYAlign(Top), mX(0), mY(0)
	{
	}
	FixedFont::~FixedFont()
	{
	}
	void FixedFont::setRegion(int left, int top, int right, int bottom)
	{
	// load font widths
	// TODO check for different device contexts
		if (mWidths.size() == 0)
		{
		// select font
			select();
		// load widths
			const uint nWidths = 256;
			Buffer<int> pWidths(nWidths);
			assert(::GetCharWidth(*this, 0, nWidths-1, pWidths));
			for (uint i = 0; i < nWidths; ++i)
				mWidths.push_back(pWidths[i]);
		}
	// set frame rectangle
		mLeft = left;
		mTop = top;
		mRight = right;
		mBottom = bottom;
		mXExtent = right-left-1;
		mYExtent = bottom-top-1;
	}
	void FixedFont::calculateWidths(const char *text, uint nChars, int *pDxs)
	{
	// calculate widths
		for (uint i = 0; i < nChars; ++i)
			pDxs[i] = mWidths[text[i]];
	}
	void FixedFont::write(const Str &text)
	{
	// output text
		write(text, mX, mY);
	}
	void FixedFont::write(const Str &text, int x, int y)
	{
	// set current position
		mX = x; mY = y;
	// check for empty string
		int nChars = text.size();
		if (nChars == 0)
			return;
	// calculate widths
		Buffer<int> pDxs(nChars);
		calculateWidths(text.c_str(), nChars, pDxs);
	// set alignment
		int total = 0;
		for (uint i = 0; i < nChars; ++i)
			total += pDxs[i];
		switch (mXAlign)
		{
		case Left:
			break;
		case Right:
			x -= total;
			break;
		case Centre:
			x -= total/2;
			break;
		}
		switch (mYAlign)
		{
		case Top:
			break;
		case Bottom:
			y -= mSize;
			break;
		case Middle:
			y -= mSize/2;
			break;
		}
	// select font
		select();
	// output text
		::ExtTextOut(*this, x, y, 0, 0, text.c_str(), nChars, pDxs);
	// update current position
		mX += total;
	}
	void FixedFont::write(const Str &text, real x, real y, int x_offset, int y_offset)
	{
	// output text
		write(text, mLeft+static_cast<int>(x*mXExtent)+x_offset, mBottom-1-static_cast<int>(y*mYExtent)+y_offset);
	}
// kerned font
	void KernedFont::setRegion(int left, int top, int right, int bottom)
	{
	// load font widths
	// TODO check for different device contexts
		if (mKernings.size() == 0)
		{
		// select font
			select();
		// load kerning pairs
			int nPairs = ::GetKerningPairs(*this, 0, 0);
			Buffer<KERNINGPAIR> pKernings(nPairs);
			::GetKerningPairs(*this, nPairs, pKernings);
			for (uint i = 0; i < nPairs; ++i)
				mKernings[join(pKernings[i].wFirst, pKernings[i].wSecond)] = pKernings[i].iKernAmount;
		}
	// set frame rectangle
		ProportionalFont::setRegion(left, top, right, bottom);
	}
	void KernedFont::calculateWidths(const char *text, uint nChars, int *pDxs)
	{
	// calculate proportional widths
		ProportionalFont::calculateWidths(text, nChars, pDxs);
	// calculate kerned
		for (uint i = 1; i < nChars; ++i)
		{
			Kernings::iterator pKerning = mKernings.find(join(text[i-1], text[i]));
			if (pKerning != mKernings.end())
				pDxs[i-1] += pKerning->second;
		}
	}
