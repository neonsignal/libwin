// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _FONT_H
#define _FONT_H
// font handle information
	class Font
	{
		public:
			Font(Ui &window, uint size, const char *name, bool bold);
			~Font();
			operator HDC() {return mWindow;}
		protected:
			void select();
		private:
			Ui &mWindow;
			HFONT mHfont;
	};
// fixed font
	class FixedFont: public Font
	{
		public:
			FixedFont(Ui &window, uint size = 16, const char *name = "Courier New", bool bold = false);
			~FixedFont();
		public:
			virtual void setRegion(int left, int top, int right, int bottom);
			void write(const Str &text);
			void write(const Str &text, int x, int y);
			void write(const Str &text, real x, real y, int x_offset = 0, int y_offset = 0);
			enum XAlign {Left, Right, Centre};
			enum YAlign {Top, Bottom, Middle};
			void setAlign(XAlign xAlign = Left, YAlign yAlign = Top) {mXAlign = xAlign; mYAlign = yAlign;}
			int size() {return mSize;}
		protected:
			virtual void calculateWidths(const char *text, uint nChars, int *pDxs);
		private:
			int mLeft, mTop, mRight, mBottom;
			int mXExtent, mYExtent;
			int mX, mY;
			std::vector<int> mWidths;
			uint mSize;
			XAlign mXAlign; YAlign mYAlign;
	};
// proportional font
	class ProportionalFont: public FixedFont
	{
		public:
			ProportionalFont(Ui &window, uint size = 16, const char *name = "Times New Roman", bool bold = false) : FixedFont(window, size, name, bold) {}
			~ProportionalFont() {}
	};
// kerned font
	class KernedFont: public ProportionalFont
	{
		public:
			KernedFont(Ui &window, uint size = 16, const char *name = "Times New Roman", bool bold = false) : ProportionalFont(window, size, name, bold) {}
			~KernedFont() {}
			virtual void setRegion(int left, int top, int right, int bottom);
		protected:
			virtual void calculateWidths(const char *text, uint nChars, int *pDxs);
		private:
			uint32_t join(char c0, char c1) {return static_cast<uint32_t>(c0)<<8 | static_cast<uint32_t>(c1);}
			typedef std::map<uint32_t, int> Kernings;
			Kernings mKernings;
	};
#endif
