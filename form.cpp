// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include <sstream>
	#include "types.h"
	#include "libwin.h"
// standard file form
	FileForm::FileForm(Window &ownerWindow, int idFileExtensions, int idDefaultFileExtension)
	{
	// get file extensions
		mFileExtensions = Resources::instance().string(idFileExtensions);
		for (int i = 0; i < mFileExtensions.size(); ++i)
			if (mFileExtensions[i] == L'\n')
				mFileExtensions[i] = 0;
		mDefaultFileExtension = Resources::instance().string(idDefaultFileExtension);
	// create file name information
		mFileName.lStructSize = sizeof(OPENFILENAME);
		mFileName.hwndOwner = ownerWindow;
		mFileName.hInstance = Resources::instance().handle();
		mFileName.lpstrFilter = mFileExtensions.c_str();
		mFileName.lpstrCustomFilter = 0; mFileName.nMaxCustFilter = 0;
		mFileName.nFilterIndex = 0;
		mFileName.lpstrFile = mName; mFileName.nMaxFile = sizeof(mName);
		mFileName.lpstrFileTitle = 0; mFileName.nMaxFileTitle = 0;
		mFileName.lpstrInitialDir = 0;
		mFileName.lpstrTitle = 0;
		mFileName.Flags = OFN_HIDEREADONLY;
		mFileName.nFileOffset = 0; mFileName.nFileExtension = 0;
		mFileName.lpstrDefExt = "txt";
		mFileName.lCustData = 0; mFileName.lpfnHook = 0;
		mFileName.lpTemplateName = 0;
		mName[0] = 0;
	}
	FileForm::~FileForm()
	{
	}
// form data items
	void TextBox::set(Window &window)
	{
		::SendDlgItemMessage(window, mId, EM_REPLACESEL, 0, (LPARAM) (LPCTSTR) mData.c_str());
	}
	void TextBox::get(Window &window)
	{
		int size = ::SendDlgItemMessage(window, mId, WM_GETTEXTLENGTH, 0, 0)+1;
		Buffer<char> pBuffer(size);
		::GetDlgItemText(window, mId, (LPTSTR) pBuffer, size);
		mData = pBuffer;
	}
	void RadioButton::set(Window &window)
	{
		HWND hChoiceItem = ::GetNextDlgGroupItem(window, GetDlgItem(window, mId), false);
		for (int choice = 0; choice != mData; ++choice)
			hChoiceItem = ::GetNextDlgGroupItem(window, hChoiceItem, false);
		::CheckDlgButton(window, ::GetDlgCtrlID(hChoiceItem), true);
	}
	void RadioButton::get(Window &window)
	{
		HWND hChoiceItem = ::GetNextDlgGroupItem(window, GetDlgItem(window, mId), false);
		int choice;
		for (choice = 0; IsDlgButtonChecked(window, ::GetDlgCtrlID(hChoiceItem)) == BST_UNCHECKED; ++choice)
			hChoiceItem = ::GetNextDlgGroupItem(window, hChoiceItem, false);
		mData = choice;
	}
	template<class T> void ComboBox<T>::set(Window &window)
	{
		std::istringstream options(Resources::instance().string(mIdOptions));
		Str option;
		while (!std::getline(options, option).fail())
			::SendDlgItemMessage(window, mId, CB_ADDSTRING, 0, (LPARAM) (LPCTSTR) option.c_str());
		std::ostringstream data;
		data << mData;
		::SendDlgItemMessage(window, mId, WM_SETTEXT, 0, (LPARAM) (data.str()).c_str());
	}
	template<class T> void ComboBox<T>::get(Window &window)
	{
		char x[50]; // TODO use allocated buffer
		::SendDlgItemMessage(window, mId, WM_GETTEXT, 50, (LPARAM) x);
		std::istringstream data(x);
		data >> mData;
	}
	template ComboBox<int>; // TODO move functions to header
	template ComboBox<uint>; // TODO move functions to header
	template ComboBox<Str>; // TODO move functions to header
	void CheckBox::set(Window &window)
	{
		::CheckDlgButton(window, mId, mData);
	}
	void CheckBox::get(Window &window)
	{
		mData = ::IsDlgButtonChecked(window, mId);
	}
	ListBox::ListBox(uint id, Data &data)
	: mId(id), mData(data)
	{
	}
	ListBox::ListBox(uint id, uint idOptions, Data &data)
	: mId(id), mOptions(Resources::instance().string(idOptions)), mData(data)
	{
	}
	void ListBox::setOptions(const Str &options)
	{
		mOptions = options;
	}
	void ListBox::set(Window &window)
	{
		std::istringstream options(mOptions);
		Str option;
		while (!std::getline(options, option).fail())
			::SendDlgItemMessage(window, mId, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) option.c_str());
		for (int index = 0; index < mData.size(); ++index)
			::SendDlgItemMessage(window, mId, LB_SETSEL, (WPARAM) true, (LPARAM) mData[index]);
	}
	void ListBox::get(Window &window)
	{
		int size = ::SendDlgItemMessage(window, mId, LB_GETSELCOUNT, 0, 0);
		Buffer<int> pBuffer(size);
		::SendDlgItemMessage(window, mId, LB_GETSELITEMS, (WPARAM) size, (LPARAM) (LPINT) pBuffer);
		mData.clear();
		for (int index = 0; index < size; ++index)
			mData.push_back(pBuffer[index]);
	}
	ListView::ListView(uint id, Data &data)
	: mId(id), mData(data)
	{
	}
	ListView::ListView(uint id, uint idOptions, Data &data)
	: mId(id), mOptions(Resources::instance().string(idOptions)), mData(data)
	{
	}
	void ListView::setOptions(const Str &options)
	{
		mOptions = options;
	}
	void ListView::set(Window &window)
	{
		::SetWindowLong(::GetDlgItem(window, mId), GWL_STYLE, ::GetWindowLong(::GetDlgItem(window, mId), GWL_STYLE)|LVS_SHOWSELALWAYS);
		std::istringstream options(mOptions);
		Str option;
		uint itemNumber = 0;
		uint index = 0;
		while (!std::getline(options, option).fail())
		{
			UINT state = 0;
			if (index < mData.size() && itemNumber == mData[index])
			{
				state |= LVIS_SELECTED;
				++index;
			}
			LV_ITEM item = {LVIF_TEXT|LVIF_STATE, itemNumber, 0, state, LVIS_SELECTED, (LPTSTR) option.c_str(), option.size(), 0, 0};
			::SendDlgItemMessage(window, mId, LVM_INSERTITEM, 0, (LPARAM) &item);
			++itemNumber;
		}
	}
	void ListView::get(Window &window)
	{
		uint items = ::SendDlgItemMessage(window, mId, LVM_GETITEMCOUNT, 0, 0);
		mData.clear();
		for (uint itemNumber = 0; itemNumber < items; ++itemNumber)
		{
			LV_ITEM item = {LVIF_STATE, itemNumber, 0, 0, LVIS_SELECTED, 0, 0, 0, 0};
			::SendDlgItemMessage(window, mId, LVM_GETITEM, 0, (LPARAM) &item);
			if (item.state & LVIS_SELECTED)
				mData.push_back(itemNumber);
		}
	}
// templated form
	TemplatedForm::TemplatedForm(Window &ownerWindow, int idTemplate)
	: mOwnerWindow(ownerWindow), mIdTemplate(idTemplate)
	{
	}
	TemplatedForm::~TemplatedForm()
	{
	}
	void TemplatedForm::add(Item &item)
	{
		mItems.push_back(&item);
	}
	bool TemplatedForm::edit()
	{
		return ::DialogBoxParam(Resources::instance().handle(), MAKEINTRESOURCE(mIdTemplate), mOwnerWindow, windowProc, reinterpret_cast<long>(this)) != 0;
	}
	BOOL CALLBACK TemplatedForm::windowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		TemplatedForm *pWindow = WinGetLong<TemplatedForm *>(hwnd);
		bool processed = false;
		if (msg == WM_INITDIALOG)
		{
			pWindow = reinterpret_cast<TemplatedForm *>(lParam);
			WinSetLong(hwnd, pWindow);
		}
		processed |= pWindow && pWindow->message(hwnd, msg, wParam, lParam);
		if (msg == WM_DESTROY)
		{
			pWindow = 0;
			WinSetLong(hwnd, pWindow);
		}
		return processed;
	}
	bool TemplatedForm::message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		Items::iterator item;
		setHwnd(hwnd);
		switch (msg)
		{
		case WM_INITDIALOG:
			for (item = mItems.begin(); item != mItems.end(); ++item)
				(*item)->set(*this);
			return false;
		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
			case IDOK:
				for (item = mItems.begin(); item != mItems.end(); ++item)
					(*item)->get(*this);
				::EndDialog(*this, 1);
				break;
			case IDCANCEL:
				::EndDialog(*this, 0);
				break;
			}
			return true;
		}
		return false;
	}
