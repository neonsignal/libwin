// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file form.h Generic Form Classes
	*/
#ifndef _FORM_H
#define _FORM_H
// include files
/** Standard File Form.
	This makes use of existing Win32 functionality to provide an file open 
	and save form.
	*/
	class FileForm: public Window
	{
	public:
		FileForm(Window &ownerWindow, int idFileExtensions, int idDefaultFileExtension);
		~FileForm();
		bool getOpen() {return ::GetOpenFileName(&mFileName) != 0;}
		bool getSave() {return ::GetSaveFileName(&mFileName) != 0;}
		char *filename() {return mName;}
	private:
		OPENFILENAME mFileName;
		char mName[256];
		Str mFileExtensions, mDefaultFileExtension;
	};
// form data items
	class Item
	{
	public:
		virtual void set(Window &window) = 0;
		virtual void get(Window &window) = 0;
	};
	class TextBox: public Item
	{
		uint mId;
		Str &mData;
	public:
		TextBox(uint id, Str &data) : mId(id), mData(data) {}
		virtual void set(Window &window);
		virtual void get(Window &window);
	};
	class RadioButton: public Item
	{
		uint mId;
		int &mData;
	public:
		RadioButton(uint id, int &data) : mId(id), mData(data) {}
		virtual void set(Window &window);
		virtual void get(Window &window);
	};
	template<class T> class ComboBox: public Item
	{
		uint mId, mIdOptions;
		T &mData;
	public:
		ComboBox(uint id, uint idOptions, T &data) : mId(id), mIdOptions(idOptions), mData(data) {}
		virtual void set(Window &window);
		virtual void get(Window &window);
	};
	class CheckBox: public Item
	{
		uint mId;
		bool &mData;
	public:
		CheckBox(uint id, bool &data) : mId(id), mData(data) {}
		virtual void set(Window &window);
		virtual void get(Window &window);
	};
	class ListBox: public Item
	{
		uint mId;
		Str mOptions;
		typedef std::vector<uint> Data;
		Data &mData;
	public:
		ListBox(uint id, Data &data);
		ListBox(uint id, uint idOptions, Data &data);
		void setOptions(const Str &options);
		virtual void set(Window &window);
		virtual void get(Window &window);
	};
	class ListView: public Item
	{
		uint mId;
		Str mOptions;
		typedef std::vector<uint> Data;
		Data &mData;
	public:
		ListView(uint id, Data &data);
		ListView(uint id, uint idOptions, Data &data);
		void setOptions(const Str &options);
		virtual void set(Window &window);
		virtual void get(Window &window);
	};
/** Templated Form.
	This is a generic form window. It contains form data items. Functionally it 
	works like a dialog box with controls.
	*/
	class TemplatedForm: public Window
	{
	public:
		typedef std::vector<Item *> Items;
	public:
		TemplatedForm(Window &ownerWindow, int idTemplate);
		virtual ~TemplatedForm();
		void add(Item &item);
		bool edit();
	private:
		Window &mOwnerWindow;
		int mIdTemplate;
		virtual bool message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		static BOOL CALLBACK windowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		Items mItems;
	};
#endif
