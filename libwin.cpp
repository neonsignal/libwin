// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include "types.h"
	#include <exdisp.h>
	#include <mshtml.h>
	#include "libwin.h"
	#include "libwin.rh"
// options form
	class OptionsForm: public TemplatedForm
	{
	public:
		OptionsForm(Window &ownerWindow);
		virtual ~OptionsForm();
	private:
		Str mD0;
		int mD1;
		Str mD2;
		bool mD3;
		std::vector<uint> mD4;
		std::vector<uint> mD6;
	private:
		TextBox mD0Item;
		RadioButton mD1Item;
		ComboBox<Str> mD2Item;
		CheckBox mD3Item;
		ListBox mD4Item;
		ListView mD6Item;
	};
	OptionsForm::OptionsForm(Window &ownerWindow)
	: TemplatedForm(ownerWindow, RdOptions),
		mD0("Demonstration"),
		mD1(0),
		mD2("none"),
		mD3(false),
		mD0Item(ReName, mD0),
		mD1Item(ReSelection, mD1),
		mD2Item(ReComboBox, RsComboBox, mD2),
		mD3Item(ReCheckBox, mD3),
		mD4Item(ReListBox, RsListBox, mD4),
		mD6Item(ReListView, RsListView, mD6)
	{
		add(mD0Item);
		add(mD1Item);
		add(mD2Item);
		add(mD3Item);
		add(mD4Item);
		add(mD6Item);
	}
	OptionsForm::~OptionsForm()
	{
	}
// text/chart pane
	class TextExamplePane: public TemplatePane
	{
	public:
		TextExamplePane();
		virtual ~TextExamplePane();
	private:
		virtual void paint();
		virtual void caret(bool create);
		virtual void key(int key);
	private:
		int mFontSize;
		char mS[10];
		KernedFont mFont;
	};
	TextExamplePane::TextExamplePane()
	: TemplatePane(),
		mFontSize(14), mFont(*this, mFontSize*2)
	{
	// initialize string
		for (int i = 0; i < sizeof(mS)-1; ++i)
			mS[i] = 'x';
		mS[sizeof(mS)-1] = 0;
	}
	TextExamplePane::~TextExamplePane()
	{
	}
	void TextExamplePane::caret(bool create)
	{
	// create caret
		if (create)
		{
			::CreateCaret(*this, 0, ::GetSystemMetrics(SM_CXBORDER), mFontSize);
			::ShowCaret(*this);
		}
	// destroy caret
		else
		{
			::HideCaret(*this);
			::DestroyCaret();
		}
	}
	void TextExamplePane::paint()
	{
	// get area
		RECT size;
		::GetClientRect(*this, &size);
	// print text
		mFont.setRegion(0,0, size.right,size.bottom);
		mFont.write("[", 90,0);
		mFont.write(mS);
		mFont.write("]");
	// move caret
		::SetCaretPos(50, 0);
	}
	void TextExamplePane::key(int key)
	{
		if (key < 0x100)
		{
			for (int i = 0; i < sizeof(mS)-2; ++i)
				mS[i] = mS[i+1];
			mS[sizeof(mS)-2] = key;
		}
		update();
	}
// browser pane
	class BrowserPane: public TemplatePane
	{
	public:
		BrowserPane();
		virtual ~BrowserPane();
		virtual bool BrowserPane::message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	public:
		void attach(TemplatedUi &templatedUi, int idEntry);
	private:
		std::auto_ptr<ComObject> mPObj;
		std::auto_ptr< ComInterface<IWebBrowser2, &IID_IWebBrowser2> > mPWebBrowser2;
	};
	bool BrowserPane::message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		// logMessage(hwnd, msg, wParam, lParam);
	// reset size TODO turn into a standard callback
		if (msg == WM_WINDOWPOSCHANGED)
		{
			RECT rect; ::GetClientRect(*this, &rect);
			(*mPWebBrowser2)->put_Width(rect.right); (*mPWebBrowser2)->put_Height(rect.bottom);
		}
	// pass message on
		return AttachedUi::message(hwnd, msg, wParam, lParam);
	}
	BrowserPane::BrowserPane()
	: TemplatePane()
	{
	// load COM control
		CLSID clsid;
		::CLSIDFromProgID(L"InternetExplorer.Application", &clsid);
		mPObj.reset(new ComObject(clsid));
		mPWebBrowser2.reset(new ComInterface<IWebBrowser2, &IID_IWebBrowser2>(*mPObj));
	// set options
		(*mPWebBrowser2)->put_FullScreen(ComBool(true));
	// navigate to URL
		(*mPWebBrowser2)->Navigate(ComStr(L"file:///e:/source/homepage/index.htm"), ComParam(0), ComParam(0), ComParam(0), ComParam(0));
	}
	BrowserPane::~BrowserPane()
	{
	}
	void BrowserPane::attach(TemplatedUi &templatedUi, int idEntry)
	{
	// attach class to dialog pane
		TemplatePane::attach(templatedUi, idEntry);
	// set up size
		RECT rect; ::GetClientRect(*this, &rect);
		(*mPWebBrowser2)->put_Width(rect.right); (*mPWebBrowser2)->put_Height(rect.bottom);
	// attach COM object to dialog pane and show
		HWND faceHwnd; (*mPWebBrowser2)->get_HWND((long *) &faceHwnd);
		::SetParent(faceHwnd, *this);
		(*mPWebBrowser2)->put_Visible(TRUE);
	// DEBUG
		::Sleep(100);
		IDispatch *pIDispatch;
		(*mPWebBrowser2)->get_Document(&pIDispatch);
		DebugStream::instance() << "dispatch=" << (int) pIDispatch << std::endl;
		IHTMLDocument2 *pIHtmlDocument2;
		pIDispatch->QueryInterface(IID_IHTMLDocument2, (void **) &pIHtmlDocument2);
		DebugStream::instance() << "htmldocument=" << (int) pIHtmlDocument2 << std::endl;
		BSTR b;
		pIHtmlDocument2->get_title(&b);
		for (int i = 0; b[i]; ++i)
			DebugStream::instance() << (char) b[i];
		DebugStream::instance() << std::endl;
	}
// main menu window
	class LibwinMainMenuFrame: public MainMenuFrame
	{
	public:
		LibwinMainMenuFrame();
		~LibwinMainMenuFrame();
	protected:
		virtual void command(int command);
	private:
		FileForm mFileForm;
		OptionsForm mOptions;
		TextExamplePane mExamplePane;
		BrowserPane mBrowserPane;
	};
	LibwinMainMenuFrame::LibwinMainMenuFrame()
	: MainMenuFrame(RdMain, RiMain),
		mOptions(*this),
		mFileForm(*this, RsFileExtensions, RsDefaultFileExtension)
	{
		mExamplePane.attach(*this, ReGraph);
		mBrowserPane.attach(*this, ReBrowser);
	}
	LibwinMainMenuFrame::~LibwinMainMenuFrame()
	{
		mBrowserPane.detach();
		mExamplePane.detach();
	}
	void LibwinMainMenuFrame::command(int command)
	{
		MainMenuFrame::command(command);
		switch (command)
		{
		case RcFileExit:
			quit();
			break;
		case RcHelpAbout:
			::MessageBox(*this, Resources::instance().string(RsHelpAbout).c_str(), Resources::instance().string(RsHelpAboutCaption).c_str(), MB_OK | MB_ICONINFORMATION);
			break;
		case RcFileOpen:
			mFileForm.getOpen();
			break;
		case RcFileSave:
			mFileForm.getSave();
			break;
		case RcOptions:
			mOptions.edit();
			break;
		}
	}
// main
	int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow)
	{
	// create resources object
		Resources resources(hInstance);
		Store store("libwin.ini");
	// create activeX object
		ActiveX activeX;
	// debug stream
		DebugStream debugStream;
	// create main window
		LibwinMainMenuFrame mainFrame;
	// message loop
		return messageLoop(mainFrame, 0);
	}
