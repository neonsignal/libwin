// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @mainpage Windows Class Library.
	@version 1.0
	@author Glenn McIntosh
	
	@section description Description
	
	This is a library to provide GUI interface classes for a Win32 application. 
	It is intended to provide functionality without code bloat, and to decouple 
	the user interface from the application code.
	
	Because of several perceived disadvantages of the MFC classes, this library 
	began as an alternative. The source code is available for enhancements. 
	Untyped pointers have been avoided (since they can result in runtime 
	errors), and macros are not used to implement messaging (since this is 
	inflexible).
	
	High level classes are decoupled from the win32 operating system, so that 
	they can potentially be reused. Base classes with public conversion 
	operators have been used to provide derived and container classes with 
	access to the handles needed by the API without forcing knowledge of those 
	handles on to every class. By using base object references instead of 
	handles to provide system specific information, porting to similar operating 
	systems requires rewriting only the base classes and any calls to the API.
	
	@section version Version History
		@subsection V1_0. V1.0
		@li
			The library source has been separated into multiple files.
		@li
			Fixed memory leak in background paint (bitmap deleted before device 
			context)
	*/
/** @file libwin.h Windows Class Library.
	*/
#ifndef _LIBWIN_H
#define _LIBWIN_H
#include "resource.h"
#include "activex.h"
#include "syswin.h"
#include "form.h"
#include "apiext.h"
#include "font.h"
#include "pen.h"
#include "chart.h"
#include "value.h"
#include "textwin.h"
#include "portio.h"
#endif
