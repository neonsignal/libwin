# GNU makefile
# windows library

# definitions
# DEBUG = defined
LIBRARY = libwin
LIBRARY_OBJ = resource.o activex.o apiext.o syswin.o form.o font.o pen.o chart.o value.o textwin.o portio.o
TEST_INC = types.h libwin.rh libwin.h resource.h activex.h apiext.h syswin.h form.h font.h pen.h chart.h value.h textwin.h portio.h
TEST_SRC = resource.cpp activex.cpp apiext.cpp libwin.cpp syswin.cpp form.cpp font.cpp pen.cpp chart.cpp value.cpp textwin.cpp portio.cpp
TEST_LIB = $(LIBRARY).coff
WFLAGS = -mwindows -lcomdlg32 -lgdi32 -luuid -lole32 -loleaut32

# standard makefile
include ../test/makefile

npuipane.dll: npuipane.o npuipane.coff $(LIBRARY).a
	dllwrap -o $@ --driver-name g++ --def npuipane.def $< npuipane.coff $(LIBRARY).a $(WFLAGS)
