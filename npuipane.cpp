// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include <inttypes.h>
	#include "types.h"
	#include "libwin.h"
	#include "npuipane.rh"
// example pane windows
	// example pane window
		class PaneWindow: public AttachedUi
		{
		public:
			PaneWindow();
			~PaneWindow();
			void update() {AttachedUi::updateBackground(); AttachedUi::update();}
		private:
			virtual void paint();
			virtual void key(int key);
		private:
			int mFontSize;
			char mS[10];
			int mX, mY;
			KernedFont mFont;
			ChartPane mChart;
		};
		PaneWindow::PaneWindow()
		: mFontSize(14), mFont(*this, mFontSize*2),
			mChart(),
			mX(0), mY(150)
		{
		// transparency
			setTransparent(true);
		// initialize string
			for (int i = 0; i < sizeof(mS)-1; ++i)
				mS[i] = 'x';
			mS[sizeof(mS)-1] = 0;
		// initialize chart
			mChart.x().push_back(100); mChart.y().push_back(200);
			mChart.x().push_back(200); mChart.y().push_back(500);
			mChart.x().push_back(500); mChart.y().push_back(400);
			mChart.x().push_back(300); mChart.y().push_back(100);
			mChart.x().push_back(200); mChart.y().push_back(-200);
			mChart.x().push_back(-100); mChart.y().push_back(0);
			mChart.x().push_back(mX); mChart.y().push_back( mY);
			mChart.zoomMax();
		}
		PaneWindow::~PaneWindow()
		{
		}
		void PaneWindow::paint()
		{
		// get area
			RECT size;
			::GetClientRect(*this, &size);
		// print text
			mFont.setRegion(0,0, size.right,size.bottom);
			mFont.write("[", 90,0);
			mFont.write(mS);
			mFont.write("]");
		// move caret
			::SetCaretPos(50, 0);
		}
		void PaneWindow::key(int key)
		{
			for (int i = 0; i < sizeof(mS)-2; ++i)
				mS[i] = mS[i+1];
			mS[sizeof(mS)-2] = key;
			mX += key>>1&1;
			if (key&1)
				mY -= key>>2&7;
			else
				mY += key>>2&7;
			mChart.x().push_back(mX); mChart.y().push_back(mY);
			switch (key)
			{
			case '1': mChart.zoomOut(); break;
			case '2': mChart.zoomIn(); break;
			case 'a': mChart.zoomLeft(); break;
			case 's': mChart.zoomRight(); break;
			case 'w': mChart.zoomUp(); break;
			case 'z': mChart.zoomDown(); break;
			}
			update();
		}
// application declarations
	extern "C"
	{
	typedef int WINAPI StartApplication(HINSTANCE hinst);
	__declspec(dllexport) StartApplication startApplication;
	}
	void endApplication();
// plugin wrapper
	// plugin api declarations
		// version
			const int PluginVersion = 0<<8 | 11;
			enum {PluginVersionHasStreamOut = 8, PluginVersionHasNotification = 9, PluginVersionHasLiveConnect = 9, PluginVersionWin16HasLiveConnect = 10};
		// basic types
			typedef unsigned char PluginBool;
		// instance data
			typedef class
			{
			public:
				PaneWindow window;
			} PluginInstance;
			typedef struct
			{
				PluginInstance *pUserData;
				void *pBrowserData;
			} InstanceData;
		// streams
			typedef struct
			{
				void *pUserData;
				void *pBrowserData;
				const char *url;
				uint32_t end;
				uint32_t lastmodified;
				void *notifyData;
			} PluginStream;
			typedef struct _PluginByteRange
			{
				int32_t offset; // negative offset is relative to end
				uint32_t length;
				struct _PluginByteRange *next;
			} PluginByteRange;
		// saved data
			typedef struct
			{
				int32_t len;
				void *buf;
			} PluginSavedData;
		// rectangle
			typedef struct
			{
				uint16_t top, left, bottom, right;
			} PluginRect;
		// window
			enum {WindowTypeWindow = 1, WindowTypeDrawable} PluginWindowType;
			typedef struct
			{
				HWND hwnd;
				uint32_t x, y;
				uint32_t xMax, yMax;
				PluginRect clipRect; // Mac only
				uint32_t type;
			} PluginWindow;
		// printing
			typedef struct
			{
				PluginBool pluginPrinted; // TRUE if plugin handled fullscreen printing
				PluginBool printOne; // TRUE if plugin should print one copy to default printer
				void *platformPrint; // printing info */
			} PluginFullPrint;
			typedef struct
			{
				PluginWindow window;
				void *platformPrint; // printing info
			} PluginEmbedPrint;
			typedef struct
			{
				uint16_t mode; // PluginFull or PluginEmbed
				union
				{
				PluginFullPrint fullPrint; // mode is PluginFull
				PluginEmbedPrint embedPrint; // mode is PluginEmbed
				} print;
			} PluginPrintInfo;
		// parameters
			enum {PluginEmbed = 1, PluginFull = 2}; // PluginNew mode
			enum {PluginNormal = 1, PluginSeek = 2, PluginAsFile = 3, PluginAsFileOnly = 4}; // PluginNewStream type
			const uint32_t PluginMaxReady = ((unsigned)(~0)<<1)>>1; // maximum stream bytes
			typedef enum {NameString = 1, DescriptionString, WindowBool, TransparentBool} PluginVariable;
			typedef enum {XDisplay = 1, XtAppContext, NetscapeWindow, JavascriptEnabledBool, AsdEnabledBool, IsOfflineBool} BrowserVariable;
		// error codes
			typedef int16_t PluginError;
			enum {PluginErrNone = 0, PluginErrGeneric, PluginErrInvalidInstance, PluginErrInvalidFuncTable, PluginErrModuleLoadFailed, PluginErrOutOfMemory, PluginErrInvalidPlugin, PluginErrInvalidPluginDir, PluginErrIncompatibleVersion, PluginErrInvalidParam, PluginErrInvalidUrl, PluginErrFileNotFound, PluginErrNoData, PluginErrStreamNotSeekable}; // PluginErr
			typedef int16_t PluginReason;
			enum {PluginResDone = 0, PluginResNetwork, PluginResUser}; // PluginReason
	// call back functions
		typedef struct
		{
			uint16_t size;
			uint16_t version;
			PluginError (*getUrl)(InstanceData *pInstanceData, const char *url, const char *pWindow);
			PluginError (*postUrl)(InstanceData *pInstanceData, const char *url, const char *pWindow, uint32_t len, const char *buf, PluginBool file);
			PluginError (*requestRead)(PluginStream *stream, PluginByteRange *rangeList);
			PluginError (*newStream)(InstanceData *pInstanceData, char *mimeType, const char *pWindow, PluginStream **stream);
			int32_t (*write)(InstanceData *pInstanceData, PluginStream *stream, int32_t len, void *buffer);
			PluginError (*destroyStream)(InstanceData *pInstanceData, PluginStream *stream, PluginReason reason);
			void (*status)(InstanceData *pInstanceData, const char *message);
			const char *(*userAgent)(InstanceData *pInstanceData);
			void *(*memAlloc)(uint32_t size);
			void (*memFree)(void *p);
			uint32_t (*memflush)(uint32_t size);
			void (*reloadPlugins)(PluginBool reloadPages);
			void (*getJavaEnv)();
			void (*getJavaPeer)();
			void *getUrlNotify;
			void *postUrlNotify;
			PluginError (*getValue)(InstanceData *pInstanceData, BrowserVariable variable, void *value);
			PluginError (*setValue)(InstanceData *pInstanceData, PluginVariable variable, void *value);
			void *invalidateRect;
			void *invalidateRegion;
			void *forceRedraw;
		} BrowserFns;
		BrowserFns *g_browserFns = 0;
	// dll entry points
		typedef struct
		{
			uint16_t size;
			uint16_t version;
			PluginError (*newInstance)(char *mimeType, InstanceData *pInstanceData, uint16_t mode, int16_t argc, char *argn[], char *argv[], PluginSavedData *saved);
			PluginError (*destroy)(InstanceData *pInstanceData, PluginSavedData **save);
			PluginError (*setWindow)(InstanceData *pInstanceData, PluginWindow *pWindow);
			PluginError (*newStream)(InstanceData *pInstanceData, char *mimeType, PluginStream *stream, PluginBool seekable, uint16_t *stype);
			PluginError (*destroyStream)(InstanceData *pInstanceData, PluginStream *stream, PluginReason reason);
			void (*streamAsFile)(InstanceData *pInstanceData, PluginStream *stream, const char *fname);
			int32_t (*writeReady)(InstanceData *pInstanceData, PluginStream *stream);
			int32_t (*write)(InstanceData *pInstanceData, PluginStream *stream, int32_t offset, int32_t len, void *buffer);
			void (*print)(InstanceData *pInstanceData, PluginPrintInfo *platformPrint);
			int16_t (*event)(InstanceData *pInstanceData, void *event);
			void (*urlNotify)(InstanceData *pInstanceData, const char *url, PluginReason reason, void *notifyData);
			void *JriGlobalRef;
			PluginError (*getValue)(InstanceData *pInstanceData, PluginVariable variable, void *value);
			PluginError (*setValue)(InstanceData *pInstanceData, BrowserVariable variable, void *value);
		} PluginFns;
		PluginError PluginNew(char *mimeType, InstanceData *pInstanceData, uint16_t mode, int16_t argc, char *argn[], char *argv[], PluginSavedData *saved)
			/*PluginNew creates a new instance of your plug-in with Mime type specified
			*by pluginType. The parameter mode is PluginEmbed if the instance was created
			*by an EMBED tag, or PluginFull if the instance was created by a separate file.
			*You can allocate any instance-specific private data in instance->pUserData at this
			*time. The pInstanceData pointer is valid until the instance is destroyed. */
		{
			for (int i = 0; i < argc; ++i)
			{
				DebugStream::instance() << i << ' ';
				if (argn[i]) DebugStream::instance() << argn[i] << ' ';
				if (argv[i]) DebugStream::instance() << argv[i] << ' ';
				DebugStream::instance() << std::endl;
			}
			PluginError result = PluginErrNone;
			if (pInstanceData == 0)
			{
				return PluginErrInvalidInstance;
			}
			pInstanceData->pUserData = new PluginInstance;
			PluginInstance *This = pInstanceData->pUserData;
			if (This == 0)
				return PluginErrOutOfMemory;
			// can use PluginSavedData to restore data from PluginDestroy
			return result;
		}
		PluginError PluginDestroy(InstanceData *pInstanceData, PluginSavedData **save)
			 /*PluginDestroy is called when a plug-in instance is deleted, typically because the
				*user has left the page containing the instance, closed the window, or quit the
				*application. You should delete any private instance-specific information stored
				*in pInstanceData->pUserData. If the instance being deleted is the last instance created
				*by your plug-in, PluginShutdown will subsequently be called, where you can
				*delete any data allocated in PluginInitialize to be shared by all your plug-in's
				*instances. Note that you should not perform any graphics operations in
				*PluginDestroy as the instance's window is no longer guaranteed to be valid. */
		{
		// get instance data
			if (pInstanceData == 0)
				return PluginErrInvalidInstance;
			PluginInstance *This = pInstanceData->pUserData;
		// delete state information
			if (This != 0)
			{
				This->window.detach();
				This->window.setHwnd(0);
				delete This;
				pInstanceData->pUserData = 0;
			}
			return PluginErrNone;
		}
		PluginError PluginSetWindow(InstanceData *pInstanceData, PluginWindow *pWindow)
		{
		// get instance data
			if (pInstanceData == 0)
				return PluginErrInvalidInstance;
			PluginInstance *This = pInstanceData->pUserData;
		// no window to use, remove old
			if (pWindow == 0 || pWindow->hwnd == 0)
			{
				This->window.detach();
				This->window.setHwnd(0);
				return PluginErrNone;
			}
		// enable messages
			if (pWindow->hwnd != static_cast<HWND>(This->window))
			{
				// ensure transparency
					HWND parent = ::GetParent(pWindow->hwnd);
					::SetWindowLong(parent, GWL_STYLE, ::GetWindowLong(parent, GWL_STYLE) & ~WS_CLIPCHILDREN);
				// attach window
					This->window.setHwnd(pWindow->hwnd);
					This->window.attach();
			}
		// redraw
			This->window.update();
			return PluginErrNone;
		}
		PluginError PluginNewStream(InstanceData *pInstanceData, char *mimeType, PluginStream *stream, PluginBool seekable, uint16_t *stype)
			 /*PluginNewStream notifies the instance denoted by instance of the creation of
				*a new stream specifed by stream. The PluginStream *pointer is valid until the
				*stream is destroyed. The Mime type of the stream is provided by the
				*parameter type. */
		{
			if (pInstanceData == 0)
				return PluginErrInvalidInstance;
			return PluginErrNone;
		}
		int32_t PluginWriteReady(InstanceData *pInstanceData, PluginStream *stream)
			 /*Returns the maximum number of bytes that an instance is prepared to accept
				*from the stream. 
				*PluginWriteReady determines the maximum number of bytes that the
				*instance will consume from the stream in a subsequent call PluginWrite. This
				*function allows the browser to only send as much data to the instance as the
				*instance is capable of handling at a time, allowing more efficient use of
				*resources within both the browser and the plug-in. */
		{
			return PluginMaxReady; // number of bytes to accept
		}
		int32_t PluginWrite(InstanceData *pInstanceData, PluginStream *stream, int32_t offset, int32_t len, void *buffer)
			 /*Delivers data from a stream and returns the number of bytes written. 
				*PluginWrite is called after a call to PluginNewStream in which the plug-in
				*requested a normal-mode stream, in which the data in the stream is delivered
				*progressively over a series of calls to PluginWriteReady and PluginWrite. The
				*function delivers a buffer buf of len bytes of data from the stream identified
				*by stream to the instance. The parameter offset is the logical position of
				*buf from the beginning of the data in the stream. */
		{
			return len;  /*number of bytes consumed, -ve means error */
		}
		PluginError PluginDestroyStream(InstanceData *pInstanceData, PluginStream *stream, PluginError reason)
			 /*Indicates the closure and deletion of a stream, and returns an error value. 
				*The PluginDestroyStream function is called when the stream identified by
				*stream for the plug-in instance denoted by instance will be destroyed. You
				*should delete any private data allocated in stream->pUserData at this time. */
		{
			if (pInstanceData == 0)
				return PluginErrInvalidInstance;
			return PluginErrNone;
		}
		void PluginStreamAsFile(InstanceData *pInstanceData, PluginStream *stream, const char *fname)
			 /*Provides a local file name for the data from a stream. 
				*PluginStreamAsFile provides the instance with a full path to a local file,
				*identified by fname, for the stream specified by stream. PluginStreamAsFile is
				*called as a result of the plug-in requesting mode PluginAsFileOnly or
				*PluginAsFile in a previous call to PluginNewStream. If an error occurs while
				*retrieving the data or writing the file, fname may be 0. */
		{
		}
		void PluginPrint(InstanceData *pInstanceData, PluginPrintInfo *printInfo)
		{
			if(printInfo == 0)
				return;
			if (pInstanceData != 0)
			// full screen
				if (printInfo->mode == PluginFull)
				{
					void *platformPrint = printInfo->print.fullPrint.platformPrint; // printer name,port,etc structure
					PluginBool printOne = printInfo->print.fullPrint.printOne; // print button used
					printInfo->print.fullPrint.pluginPrinted = FALSE; // defer to embedded
				}
			// embedded
				else
				{
					PluginWindow *printWindow = &(printInfo->print.embedPrint.window); // location and dimensions
					void *platformPrint = printInfo->print.embedPrint.platformPrint; // device context
				}
		}
		int16_t PluginEvent(InstanceData *pInstanceData, void *event)
		{
		// not used
			return 0; // not handled
		}
		void PluginURLNotify(InstanceData *pInstanceData, const char *url, PluginReason reason, void *notifyData)
		{
		// not used
		}
	// global entry points
		extern "C"
		{
		// load entry points
			__declspec(dllexport) PluginError WINAPI NP_GetEntryPoints(PluginFns *pFuncs)
			{
				if(pFuncs == 0)
					return PluginErrInvalidFuncTable;
				pFuncs->version = PluginVersion;
				pFuncs->newInstance = PluginNew;
				pFuncs->destroy = PluginDestroy;
				pFuncs->setWindow = PluginSetWindow;
				pFuncs->newStream = PluginNewStream;
				pFuncs->destroyStream = PluginDestroyStream;
				pFuncs->streamAsFile = PluginStreamAsFile;
				pFuncs->writeReady = PluginWriteReady;
				pFuncs->write = PluginWrite;
				pFuncs->print = PluginPrint;
				pFuncs->event = PluginEvent;
				pFuncs->urlNotify = PluginURLNotify;
				pFuncs->JriGlobalRef = 0;
				pFuncs->getValue = 0;
				pFuncs->setValue = 0;
				return PluginErrNone;
			}
		// initialize
			__declspec(dllexport) PluginError WINAPI NP_Initialize(BrowserFns *pFuncs)
			{
			// create application if not already going
				const char *moduleName = "c:\\programs\\opera\\plugins\\npuipane.dll";
				if (::GetModuleHandle(moduleName) != 0)
				{
					HINSTANCE hinst = ::LoadLibrary(moduleName);
					StartApplication *pStartApplication = reinterpret_cast<StartApplication *>(::GetProcAddress(hinst, "startApplication@4"));
					(*pStartApplication)(hinst);
				}
			// setup plugin API function table
				if(pFuncs == 0)
					return PluginErrInvalidFuncTable;
				g_browserFns = pFuncs;
				DebugStream::instance() << "version" << ' ' << (int) HIBYTE(pFuncs->version) << '.' << (int) LOBYTE(pFuncs->version) << std::endl;
				if(HIBYTE(pFuncs->version) > HIBYTE(PluginVersion))
					return PluginErrIncompatibleVersion;
				return PluginErrNone;
			}
		// shutdown
			__declspec(dllexport) PluginError WINAPI NP_Shutdown()
			{
			// clear plugin API function table
				g_browserFns = 0;
				return PluginErrNone;
			}
		}
	// dll main
		BOOL WINAPI DllMain(HINSTANCE hinstDll, DWORD fdwReason, LPVOID lpvReserved)
		{
			switch(fdwReason) 
			{ 
			case DLL_PROCESS_ATTACH:
				break;
			case DLL_THREAD_ATTACH:
				break;
			case DLL_THREAD_DETACH:
				break;
			case DLL_PROCESS_DETACH:
				break;
			}
			return TRUE; // Successful DLL_PROCESS_ATTACH.
		}
// application
	class Application: public Singleton<Application>
	{
	private:
		DebugStream debugStream;
	};
	__declspec(dllexport) int WINAPI startApplication(HINSTANCE hinst)
	{
		new Resources(hinst);
		new Application;
		::atexit(endApplication);
		return 0;
	}
	void endApplication()
	{
		delete &Application::instance();
		delete &Resources::instance();
	}
