// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include <sstream>
	#include "types.h"
	#include "libwin.h"
// pens
	// pen handle information
		Pen::Pen(Ui &window, bool solid, uint16_t color)
		: mWindow(window)
		{
			LOGBRUSH logBrush;
			logBrush.lbStyle = BS_SOLID;
			logBrush.lbColor = color;
			logBrush.lbHatch = 0;
			mHpen = ::ExtCreatePen(PS_COSMETIC | (solid?PS_SOLID:PS_DOT), 1, &logBrush, 0, 0);
		}
		Pen::~Pen()
		{
			::DeleteObject(mHpen);
		}
		void Pen::select()
		{
			::SelectObject(*this, mHpen);
		}
	// line pen
		void LinePen::setRegion(int left, int top, int right, int bottom)
		{
		// set frame rectangle
			mLeft = left;
			mTop = top;
			mRight = right;
			mBottom = bottom;
			mXExtent = right-left-1;
			mYExtent = bottom-top-1;
		// clear data
			mPX.clear();
			mPY.clear();
			mPN.clear();
			mNew = true;
		}
		real LinePen::xPos(int x)
		{
		// unscale x
			real xNorm = (x-mLeft)*1./mXExtent;
			if (xNorm < 0.) xNorm = 0.;
			if (xNorm > 1.) xNorm = 1.;
			return xNorm;
		}
		real LinePen::yPos(int y)
		{
		// unscale y
			real yNorm = (mBottom-1-y)*1./mYExtent;
			if (yNorm < 0.) yNorm = 0.;
			if (yNorm > 1.) yNorm = 1.;
			return yNorm;
		}
		real LinePen::xScale(int x)
		{
		// unscale x
			return x*1./mXExtent;
		}
		real LinePen::yScale(int y)
		{
		// unscale y
			return y*1./mYExtent;
		}
		void LinePen::add(int x, int y)
		{
		// new x value
			if (mNew || x != mX0)
			{
				if (!mNew)
					flush();
				append(x,y);
				mX0 = x;
				mY0 = mYMin = mYMax = y;
				mNew = false;
			}
		// else same x value
			else
			{
				if (y > mYMax) mYMax = y;
				if (y < mYMin) mYMin = y;
				mY1 = y;
			}
		}
		void LinePen::add(real x, real y)
		{
		// add value
			add(static_cast<int>(x * mXExtent), static_cast<int>(y * mYExtent));
		}
		void LinePen::close()
		{
		// terminate current curve
			if (!mNew)
			{
				flush();
				mPN.push_back(mPX.size());
				mNew = true;
			}
		}
		void LinePen::flush()
		{
		// if more than one point
			if (mYMin == mYMax)
				return;
		// generate range of points from previous x value
			if (mY0 >= mY1)
			{
				if (mYMax > mY0)
					append(mX0, mYMax);
				if (mYMin < mY1)
					append(mX0, mYMin);
			}
			else // mY0 < mY1
			{
				if (mYMin < mY0)
					append(mX0, mYMin);
				if (mYMax > mY1)
					append(mX0, mYMax);
			}
			append(mX0, mY1);
		}
		void LinePen::draw()
		{
			int nCurves = mPN.size();
			if (nCurves == 0) return;
			int nPoints = mPN[nCurves-1];
			Buffer<POINT> pPoints(nPoints);
			int i;
			for (i = 0; i < nPoints; ++i)
			{
				pPoints[i].x = mLeft+mPX[i];
				pPoints[i].y = mBottom-1-mPY[i];
			}
			select();
			i = 0;
			for (int iCurves = 0; iCurves < nCurves; ++iCurves)
			{
				int n = mPN[iCurves]-i;
				if (n >= 2)
					::Polyline(*this, pPoints+i, n);
				i += n;
				if (n >= 1)
				// TODO wrong color
					::SetPixel(*this, pPoints[i-1].x, pPoints[i-1].y, 0);
			}
		}
	// cursor pen
		void CursorPen::setRegion(int left, int top, int right, int bottom)
		{
		// set frame rectangle
			mLeft = left;
			mTop = top;
			mRight = right;
			mBottom = bottom;
			mXExtent = right-left-1;
			mYExtent = bottom-top-1;
		}
		void CursorPen::draw(real x0, real y0, real x1, real y1)
		{
		// undraw old cursor
			draw();
			mNew = false;
		// rescale to pixel coordinates
			mPPoints[0].x = mLeft + static_cast<int16_t>(x0*mXExtent);
			mPPoints[0].y = mBottom-1 - static_cast<int16_t>(y0*mYExtent);
			mPPoints[2].x = mLeft + static_cast<int16_t>(x1*mXExtent);
			mPPoints[2].y = mBottom-1 - static_cast<int16_t>(y1*mYExtent);
		// setup points
			mPPoints[1].x = mPPoints[0].x; mPPoints[1].y = mPPoints[2].y;
			mPPoints[3].x = mPPoints[2].x; mPPoints[3].y = mPPoints[0].y;
			mPPoints[4].x = mPPoints[0].x; mPPoints[4].y = mPPoints[0].y;
		// draw
			draw();
		}
		void CursorPen::draw()
		{
		// redraw old cursor
			if (!mNew)
			{
			// draw
				select();
				::SetROP2(*this, R2_NOTXORPEN);
				::Polyline(*this, mPPoints, 5);
				::SetROP2(*this, R2_COPYPEN);
			}
		}
		void CursorPen::close()
		{
		// undraw old cursor
			draw();
			mNew = true;
		}
