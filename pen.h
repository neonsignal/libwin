// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _PEN_H
#define _PEN_H
// pen handle information
	class Pen
	{
		public:
			Pen(Ui &window, bool solid = true, uint16_t color = RGB(0x00,0x00,0x00));
			~Pen();
			operator HDC() {return mWindow;}
		protected:
			void select();
		private:
			Ui &mWindow;
			HPEN mHpen;
	};
// line pen
	class LinePen: public Pen
	{
	public:
		LinePen(Ui &window, uint16_t color = RGB(0x00,0x00,0x00)) : Pen(window, true, color), mNew(true) {}
		~LinePen() {}
	public:
		void setRegion(int left, int top, int right, int bottom);
		real xPos(int x);
		real yPos(int y);
		real xScale(int x);
		real yScale(int y);
		void add(int x, int y);
		void add(real x, real y);
		void close();
		void draw();
	private:
		void append(int x, int y) {mPX.push_back(x); mPY.push_back(y);}
		void flush();
		std::vector<int> mPX;
		std::vector<int> mPY;
		std::vector<int> mPN;
		bool mNew;
		int mX0, mY0, mYMin, mYMax, mY1;
		int mLeft, mTop, mRight, mBottom;
		real mXExtent, mYExtent;
	};
// cursor pen
	class CursorPen: public Pen
	{
	public:
		CursorPen(Ui &window) : Pen(window, false, RGB(0x80,0x80,0x80)), mNew(true) {}
		~CursorPen() {}
	public:
		void setRegion(int left, int top, int right, int bottom);
		void draw(real x0, real y0, real x1, real y1);
		void draw();
		void close();
	private:
		bool mNew;
		int mLeft, mTop, mRight, mBottom;
		real mXExtent, mYExtent;
		int mX0, mY0, mX, mY;
		POINT mPPoints[5];
	};
#endif
