// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include <sstream>
	#include "types.h"
	#include "libwin.h"
// com port
	int ComPort::getc()
	{
		uint8_t c = 0;
		DWORD nRead;
		::ReadFile(mHandle, &c, 1, &nRead, 0);
		if (nRead == 0)
			throw RxTimeout;
		return c;
	}
	void ComPort::putc(int c)
	{
		uint8_t c1 = c;
		DWORD nWritten;
		::WriteFile(mHandle, &c1, 1, &nWritten, 0);
		if (nWritten == 0)
			throw TxTimeout;
	}
	bool ComPort::open(int port, int baudRate)
	{
	// open channel
		std::ostringstream name;
		name << "COM" << port;
		mHandle = CreateFile(name.str().c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
		if (mHandle == INVALID_HANDLE_VALUE) return false;
	// initialize channel parameters
		DCB dcb;
		GetCommState(mHandle, &dcb);
		dcb.DCBlength = sizeof(DCB);
		dcb.BaudRate = baudRate;
		dcb.fBinary = TRUE;
		dcb.fParity = FALSE;
		dcb.fOutxCtsFlow = FALSE;
		dcb.fOutxDsrFlow = FALSE;
		dcb.fDtrControl = DTR_CONTROL_ENABLE;
		dcb.fDsrSensitivity = FALSE;
		dcb.fOutX = dcb.fInX = false;
		dcb.fNull = FALSE;
		dcb.fRtsControl = RTS_CONTROL_ENABLE;
		dcb.fAbortOnError = FALSE;
		dcb.XonLim = dcb.XoffLim = 0x0A;
		dcb.ByteSize = 8;
		dcb.Parity = NOPARITY;
		dcb.StopBits = ONESTOPBIT;
		dcb.XonChar = 0x11; dcb.XoffChar = 0x13;
		SetCommState(mHandle, &dcb);
	// set timeouts
		COMMTIMEOUTS commTimeouts;
		::GetCommTimeouts(mHandle, &commTimeouts);
		commTimeouts.WriteTotalTimeoutMultiplier = 0;
		commTimeouts.WriteTotalTimeoutConstant = 100;
		commTimeouts.ReadIntervalTimeout = 0;
		commTimeouts.ReadTotalTimeoutMultiplier = 0;
		commTimeouts.ReadTotalTimeoutConstant = 100;
		::SetCommTimeouts(mHandle, &commTimeouts);
	// flush
		::PurgeComm(mHandle, PURGE_RXCLEAR);
	// return
		return true;
	}
	void ComPort::flush()
	{
		::PurgeComm(mHandle, PURGE_RXCLEAR);
	}
	void ComPort::close()
	{
		if (mHandle != INVALID_HANDLE_VALUE)
			::CloseHandle(mHandle);
	}
