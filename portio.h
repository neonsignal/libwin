// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _PORTIO_H
#define _PORTIO_H
// com port
	class Port
	{
	public:
		Port() {}
		virtual ~Port() {};
		typedef enum {RxTimeout, TxTimeout} Exception;
	public:
		virtual int getc() {return 0;}
		virtual void putc(int c) {}
		virtual void flush() {}
	};
	class ComPort: public Port
	{
	public:
		ComPort() : mHandle(INVALID_HANDLE_VALUE) {}
		~ComPort() {}
	public:
		bool open(int port = 1, int baudRate = 9600);
		void close();
	public:
		virtual int getc();
		virtual void putc(int c);
		virtual void flush();
	private:
		HANDLE mHandle;
	};
// com port stream
	template<class T> class PortStreamBuffer: public std::basic_streambuf<T>
	{
	public:
		typedef typename std::char_traits<T>::int_type int_type;
		PortStreamBuffer(Port &port);
		~PortStreamBuffer();
	protected:
		virtual int sync();
		virtual int_type underflow();
		virtual int_type overflow(int_type c = std::char_traits<T>::eof());
	private:
		Port &mPort;
		char mInput;
	};
	template<class T> PortStreamBuffer<T>::PortStreamBuffer(Port &port)
	: mPort(port)
	{
		setg(&mInput+1, &mInput+1, &mInput+1);
		mPort.flush();
	}
	template<class T> PortStreamBuffer<T>::~PortStreamBuffer()
	{
	}
	template<class T> typename PortStreamBuffer<T>::int_type PortStreamBuffer<T>::underflow()
	{
		mInput = mPort.getc();
		setg(&mInput, &mInput, &mInput+1);
		return mInput;
	}
	template<class T> typename PortStreamBuffer<T>::int_type PortStreamBuffer<T>::overflow(int_type c)
	{
		mPort.putc(c);
		return c;
	}
	template<class T> int PortStreamBuffer<T>::sync()
	{
		mPort.flush();
		return 0;
	}
	template<class T> class PortStream: public std::basic_iostream<T>
	{
	public:
		PortStream(Port &port) : buffer(port), std::basic_iostream<T>(&buffer) {}
		~PortStream() {}
	private:
		PortStreamBuffer<T> buffer;
	};
#endif
