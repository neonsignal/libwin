// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include "types.h"
	#include "libwin.h"
// resources
	Resources::Resources(HINSTANCE hInstance)
	: mHInstance(hInstance)
	{
	// setup and register generic pane type
		WNDCLASS wndclass;
		wndclass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wndclass.lpfnWndProc = &::DefWindowProc;
		wndclass.cbClsExtra = 0;
		wndclass.cbWndExtra = 0;
		wndclass.hInstance = mHInstance;
		wndclass.hIcon = 0;
		wndclass.hCursor = ::LoadCursor(0, IDC_ARROW);
		wndclass.hbrBackground = 0;
		wndclass.lpszClassName = "GenericPane";
		wndclass.lpszMenuName = 0;
		::RegisterClass(&wndclass);
	// create ui access mutext
		mUiAccess = ::CreateMutex(0, FALSE, 0);
	}
	Resources::~Resources()
	{
	// unregister generic pane type
		::UnregisterClass("GenericPane", mHInstance);
	}
	void Resources::uiAccess(bool access)
	{
		if (access)
			::WaitForSingleObject(mUiAccess, INFINITE);
		else
			::ReleaseMutex(mUiAccess);
	}
	Str Resources::string(uint id)
	{
		const int size = 512; // same as Micro$oft's CString::LoadString
		Buffer<char> pBuffer(size);
		::LoadString(mHInstance, id, pBuffer, size);
		Str data(pBuffer);
		return data;
	}
	HICON Resources::icon(uint id)
	{
		return ::LoadIcon(mHInstance, MAKEINTRESOURCE(id));
	}
// permanent data store
	Store::Store(const Str filename)
	{
		mFilename = ".\\"+filename;
		const uint MaxLength = 32767;
		Buffer<char> buffer(MaxLength);
		::GetPrivateProfileSection("PermanentStore", buffer, MaxLength, mFilename.c_str());
		char *pBuffer = buffer;
		while (*pBuffer)
		{
			char *pKey = pBuffer;
			while (*pBuffer != '=')
				++pBuffer;
			*pBuffer = 0;
			++pBuffer;
			char *pValue = pBuffer;
			while (*pBuffer)
				++pBuffer;
			++pBuffer;
			mStore[pKey] = pValue;
		}
	}
	Store::~Store()
	{
		const uint MaxLength = 32767;
		Buffer<char> buffer(MaxLength);
		char *pBuffer = buffer;
		*pBuffer = 0;
		::WritePrivateProfileSection("PermanentStore", buffer, mFilename.c_str());
	}
	Str Store::string(const Str id)
	{
	}
