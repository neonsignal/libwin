// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _RESOURCE_H
#define _RESOURCE_H
/** Resources.
	This singleton provides global access to the 'resource' database. 
	Resources in this context mean GUI objects that can be used by operating 
	system API calls, and which are linked into the application at compile time. 
	They are accessed on the basis of unique identifying integers.
	*/
	class Resources: public Singleton<Resources>
	{
	public:
		Resources(HINSTANCE hInstance);
		~Resources();
	public:
		HINSTANCE handle() {return mHInstance;}
		void uiAccess(bool access);
	public:
		Str string(uint id);
		HICON icon(uint id);
	private:
		HINSTANCE mHInstance;
		HANDLE mUiAccess;
	};
/** User Interface Access.
	This provides protection of the user interface. Rather than generate a new 
	mutex object for every interaction between a thread and the user interface, 
	this object is used by convention. Normally this would be created on the 
	stack for the duration of the access. It is used to protect the message 
	handler (so the thread responsible for normal user interaction should not 
	create this object within a user interface method). Typically it would be 
	used by other threads while they are updating the user interface outside of 
	the message handler).
	*/
	class UiAccess
	{
	public:
		UiAccess() {Resources::instance().uiAccess(true);}
		~UiAccess() {Resources::instance().uiAccess(false);}
	};
#endif
/** Permanent Data Access.
	This provides access to a permanent data store. This is loaded when the 
	object is created, and saved when it is deleted. The data is accessed on 
	the basis of unique identifying strings.
	*/
	class Store: public Singleton<Store>
	{
	public:
		Store(const Str filename);
		~Store();
	public:
		Str string(const Str id);
	private:
		Str mFilename;
		std::map<Str, Str> mStore;
	};
