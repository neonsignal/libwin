// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include <process.h>
	#include <iomanip>
	#include <sstream>
	#include "types.h"
	#include "libwin.h"
// generic user interface
	Ui::Ui()
	: mHdc(0), mTransparent(false)
	{
	}
	Ui::~Ui()
	{
	}
	void Ui::initContext()
	{
		mHdc = ::GetDC(*this);
		WinSetLong(*this, this);
	}
	void Ui::deinitContext()
	{
		WinSetLong(*this, static_cast<Ui *>(0));
		::ReleaseDC(*this, *this); mHdc = 0;
	}
	bool Ui::message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_DEFERSHOWWINDOW:
			::ShowWindow(*this, SW_SHOWNOACTIVATE);
			return true;
		case WM_CLOSE:
			::DestroyWindow(*this);
			return true;
		case WM_SETFOCUS:
			caret(true);
			return true;
		case WM_KILLFOCUS:
			caret(false);
			return true;
		case WM_ERASEBKGND:
			return true;
		case WM_PAINT:
			{
			// begin paint
				PAINTSTRUCT paintStruct;
				::BeginPaint(*this, &paintStruct);
			// save old device context
				HDC hdc0 = mHdc;
			// create hidden device context
				RECT rect;
				::GetClientRect(*this, &rect);
				HBITMAP hBitmap = ::CreateCompatibleBitmap(paintStruct.hdc, rect.right-rect.left, rect.bottom-rect.top);
				mHdc = ::CreateCompatibleDC(paintStruct.hdc);
				::SelectObject(mHdc, hBitmap);
			// erase background
				HBRUSH hBrush = ::GetSysColorBrush(COLOR_WINDOW);
				::FillRect(mHdc, &rect, hBrush);
			// paint foreground
				::SetBkMode(mHdc, TRANSPARENT);
				paint();
			// copy hidden device context to window
				::BitBlt(paintStruct.hdc, rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top, mHdc, 0, 0, mTransparent?SRCAND:SRCCOPY);
			// delete objects
				::DeleteDC(mHdc);
				::DeleteObject(hBitmap);
			// restore old device context
				mHdc = hdc0;
			// end paint
				::EndPaint(*this, &paintStruct);
			}
			return true;
		case WM_KEYDOWN:
			{
				int code = wParam;
				if (0 <= code && code < 0x30)
				key(code|0x100);
			}
			return true;
		case WM_CHAR:
			if (wParam)
				key(static_cast<int>(wParam));
			return true;
		case WM_LBUTTONDOWN:
			::SetFocus(*this);
			::SetCapture(*this);
			pointer(MAKEPOINTS(lParam).x, MAKEPOINTS(lParam).y, true, true);
			return true;
		case WM_LBUTTONUP:
			pointer(MAKEPOINTS(lParam).x, MAKEPOINTS(lParam).y, true, false);
			::ReleaseCapture();
			return true;
		case WM_MOUSEMOVE:
			pointer(MAKEPOINTS(lParam).x, MAKEPOINTS(lParam).y, false);
			return true;
		}
		return false;
	}
	void Ui::show()
	{
		::PostMessage(*this, WM_DEFERSHOWWINDOW, 0, 0);
	}
	void Ui::update()
	{
		::InvalidateRect(*this, 0, true);
	}
	void Ui::updateBackground()
	{
		HWND parent = ::GetParent(*this);
		if (parent)
		{
			RECT rect, rect0;
			::GetWindowRect(*this, &rect);
			::GetWindowRect(parent, &rect0);
			rect.left -= rect0.left; rect.top -= rect0.top;
			rect.right -= rect0.left; rect.bottom -= rect0.top;
			::InvalidateRect(parent, &rect, true);
		}
	}
	void Ui::initMessage()
	{
		initContext();
		mPDefaultWindowProc = WinSetLong<WindowProc *>(*this, &windowProc, GWL_WNDPROC);
	}
	void Ui::deinitMessage()
	{
		WinSetLong(*this, mPDefaultWindowProc, GWL_WNDPROC);
		deinitContext();
	}
	LRESULT CALLBACK Ui::windowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		Ui *pWindow = WinGetLong<Ui *>(hwnd);
		assert(pWindow);
		if (msg == WM_GETDLGCODE) // TODO move this into the templated pane code
			return DLGC_WANTCHARS | DLGC_WANTARROWS;
		if (!pWindow->message(hwnd, msg, wParam, lParam))
			return (*pWindow->mPDefaultWindowProc)(hwnd, msg, wParam, lParam);
		return 0;
	}
// attached user interface
	void AttachedUi::attach()
	{
		initMessage();
	}
	void AttachedUi::detach()
	{
		deinitMessage();
	}
// framed user interface
	StandaloneUi::StandaloneUi(Window *pOwnerWindow, const Str &caption, uint idIcon, WORD menu)
	{
	// setup class name
		std::ostringstream className;
		className << std::resetiosflags(std::ios::basefield) << std::setiosflags(std::ios::hex | std::ios::uppercase) << std::setfill('0');
		className << "Interface " << std::setw(8) << _getpid() << '.' << std::setw(8) << reinterpret_cast<int>(this);
		mClassName = className.str();
	// setup and register class type
		WNDCLASS wndclass;
		wndclass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wndclass.lpfnWndProc = &::DefWindowProc;
		wndclass.cbClsExtra = 0;
		wndclass.cbWndExtra = 0;
		wndclass.hInstance = Resources::instance().handle();
		wndclass.hIcon = Resources::instance().icon(idIcon);
		wndclass.hCursor = ::LoadCursor(0, IDC_ARROW);
		wndclass.hbrBackground = 0;
		wndclass.lpszClassName = mClassName.c_str();
		wndclass.lpszMenuName = MAKEINTRESOURCE(menu);
		::RegisterClass(&wndclass);
	// create window
		HWND owner = 0;
		if (pOwnerWindow)
			owner = *pOwnerWindow;
		HWND hwnd;
		assert(hwnd = ::CreateWindow(mClassName.c_str(), caption.c_str(), WS_TILEDWINDOW, CW_USEDEFAULT,0, CW_USEDEFAULT,0, owner, 0, Resources::instance().handle(), 0));
		setHwnd(hwnd);
	// initialize message procedure
		initMessage();
	// show will be deferred until windows constructor returns
		show();
	}
	StandaloneUi::~StandaloneUi()
	{
	// uninitialize message procedure
		deinitMessage();
	// remove class
		::DestroyWindow(*this);
		::UnregisterClass(mClassName.c_str(), Resources::instance().handle());
	}
// templated user interface
	TemplatedUi::TemplatedUi(Window *pOwnerWindow, int idTemplate, int idIcon)
	{
	// create window
		HWND owner = 0;
		if (pOwnerWindow)
			owner = *pOwnerWindow;
		HWND hwnd;
		assert(hwnd = ::CreateDialogParam(Resources::instance().handle(), MAKEINTRESOURCE(idTemplate), pOwnerWindow ? (HWND) *pOwnerWindow : 0, &windowProc, reinterpret_cast<long>(this)));
		RECT rect;
		::GetWindowRect(hwnd, &rect);
		mSizeX = rect.right-rect.left; mSizeY = rect.bottom-rect.top;
		mSizeXInitial = mSizeX; mSizeYInitial = mSizeY;
		mDeltaCx = mDeltaCy = 0;
		::SetClassLong(hwnd, GCL_HICON, reinterpret_cast<long>(Resources::instance().icon(idIcon)));
		setHwnd(hwnd);
	}
	TemplatedUi::~TemplatedUi()
	{
	// remove class
		::DestroyWindow(*this);
	}
	bool TemplatedUi::message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_SIZING:
			{
			// prevent resize less than initial dialog size
				RECT &rect = *reinterpret_cast<RECT *>(lParam);
				if (rect.right-rect.left < mSizeXInitial) rect.right = rect.left+mSizeXInitial;
				if (rect.bottom-rect.top < mSizeYInitial) rect.bottom = rect.top+mSizeYInitial;
			}
			break;
		case WM_WINDOWPOSCHANGED:
			{
			// calculate the resize shift
				WINDOWPOS &windowPos = *reinterpret_cast<LPWINDOWPOS>(lParam);
				if (windowPos.flags & (SWP_NOCOPYBITS|SWP_NOSIZE)) break;
				mDeltaCx = windowPos.cx - mSizeX;
				mDeltaCy = windowPos.cy - mSizeY;
			// resize child panes
				::EnumChildWindows(*this, enumChildProc, reinterpret_cast<LPARAM>(this));
			// save new rectangle
				mSizeX = windowPos.cx; mSizeY = windowPos.cy;
			}
			break;
		case WM_CLOSE:
			::DestroyWindow(*this);
			break;
		}
		return false;
	}
	BOOL CALLBACK TemplatedUi::windowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		TemplatedUi *pWindow = WinGetLong<TemplatedUi *>(hwnd);
		bool processed = false;
		if (msg == WM_INITDIALOG)
		{
			pWindow = reinterpret_cast<TemplatedUi *>(lParam);
			WinSetLong(hwnd, pWindow);
		}
		processed |= pWindow && pWindow->message(hwnd, msg, wParam, lParam);
		if (msg == WM_DESTROY)
		{
			pWindow = 0;
			WinSetLong(hwnd, pWindow);
		}
		return processed;
	}
	void TemplatedUi::resizeChild(HWND hwnd)
	{
	// ignore if not immediate child
		if (::GetParent(hwnd) != *this)
			return;
	// get child dimensions
		RECT rect;
		::GetWindowRect(hwnd, &rect);
		int sizeX = rect.right-rect.left;
		int sizeY = rect.bottom-rect.top;
		POINT point = {rect.left, rect.top};
		::ScreenToClient(*this, &point);
	// shift
		if (point.x > mSizeX/2)
			point.x += mDeltaCx;
		if (point.y > mSizeY/2)
			point.y += mDeltaCy;
	// resize
		LONG style = ::GetWindowLong(hwnd, GWL_STYLE);
		if (rect.right-rect.left > mSizeX/2)
			sizeX += mDeltaCx;
		if (rect.bottom-rect.top > mSizeY/2)
			sizeY += mDeltaCy;
	// shift window
		::SetWindowPos(hwnd, 0, point.x, point.y, sizeX, sizeY, SWP_NOREPOSITION);
	}
	BOOL CALLBACK TemplatedUi::enumChildProc(HWND hwnd, LPARAM lParam)
	{
		TemplatedUi *pWindow = reinterpret_cast<TemplatedUi *>(lParam);
		pWindow->resizeChild(hwnd);
		return TRUE;
	}
// template control
	TemplateControl::TemplateControl()
	{
	}
	TemplateControl::~TemplateControl()
	{
	}
	void TemplateControl::attach(TemplatedUi &templatedUi, int idEntry)
	{
		setHwnd(templatedUi);
		mIdEntry = idEntry;
	}
	void TemplateControl::detach()
	{
	}
	void TemplateControl::setText(const Str &text)
	{
		::SetDlgItemText(*this, mIdEntry, text.c_str());
	}
// template pane
	TemplatePane::TemplatePane()
	{
	}
	TemplatePane::~TemplatePane()
	{
	}
	void TemplatePane::attach(TemplatedUi &templatedUi, int idEntry)
	{
		setHwnd(::GetDlgItem(templatedUi, idEntry));
		AttachedUi::attach();
		update();
	}
// menu frame
	void MenuFrame::command(int command)
	{
	}
	bool MenuFrame::message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		if (msg == WM_COMMAND)
		{
			command(LOWORD(wParam));
			return true;
		}
		return TemplatedUi::message(hwnd, msg, wParam, lParam);
	}
// main menu frame
	MainMenuFrame::MainMenuFrame(uint idTemplate, uint idIcon)
	: MenuFrame(idTemplate, idIcon)
	{
	}
	MainMenuFrame::~MainMenuFrame()
	{
	}
	void MainMenuFrame::quit()
	{
		::PostQuitMessage(0);
	}
	bool MainMenuFrame::message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		if (msg == WM_DESTROY)
			quit();
		return MenuFrame::message(hwnd, msg, wParam, lParam);
	}
