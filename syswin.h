// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file syswin.h Generic Window Classes.
	*/
#ifndef _SYSWIN_H
#define _SYSWIN_H
/** Window Handle.
	This base class makes a window handle available implicitly to any classes 
	that have a reference to a Window object, so that operating system handles 
	can be carried around invisible to classes that do not require them.
	*/
	class Window
	{
	public:
		operator HWND() {return mHwnd;}
		void setHwnd(HWND hwnd) {mHwnd = hwnd;}
	private:
		HWND mHwnd;
	};
/** Generic User Interface.
	This is the base class for all interface windows that interact with the 
	application on the basis of callback messages (as distinct from 
	templated windows which are based on panes or templated forms which are 
	based on entry/exit transactions). It also makes the device context 
	available implicitly to any classes that have a reference to a Ui object.
	*/
	class Ui: public Window
	{
	public:
		Ui();
		virtual ~Ui();
	public:
		operator HDC() {return mHdc;}
	protected:
		void initContext();
		void deinitContext();
		void initMessage();
		void deinitMessage();
		virtual bool message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		virtual void show();
		virtual void update();
		virtual void updateBackground();
		virtual void setTransparent(bool transparent) {mTransparent = transparent;}
	private:
		virtual void paint() {}
		virtual void caret(bool create) {}
		virtual void key(int key) {}
		virtual void pointer(int x, int y, bool button = false, bool active = false) {}
	private:
		typedef LRESULT CALLBACK WindowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		static WindowProc windowProc;
		WindowProc *mPDefaultWindowProc;
	private:
		HDC mHdc;
		enum {WM_DEFERSHOWWINDOW = WM_USER};
		bool mTransparent;
	};
/** Attached User Interface.
	This acts as an interface to an operating system window object that 
	is created and destroyed independently of this class. It can be 
	repeatedly attached to and detached from different window objects.
	*/
	class AttachedUi: public Ui
	{
	public:
		void attach();
		void detach();
	};
/** Standalone User Interface.
	This acts as an interface to an operating system window object, 
	and controls the creation and destruction of that object.
	*/
	class StandaloneUi: public Ui
	{
	public:
		StandaloneUi(Window *pOwnerWindow, const Str &caption, uint idIcon = 0, WORD menu = 0);
		virtual ~StandaloneUi();
	private:
		Str mClassName;
	};
/** Templated User Interface.
	This acts as an interface to an operating system window that is templated 
	(typically using a dialog resource).
	*/
	class TemplatedUi: public Window
	{
	public:
		TemplatedUi(Window *pOwnerWindow, int idTemplate, int idIcon);
		virtual ~TemplatedUi();
	public:
		enum {WS_RESIZABLEX = 0x0002, WS_RESIZABLEY = 0x0001};
	protected:
		virtual bool message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	private:
		static BOOL CALLBACK windowProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	private:
		void resizeChild(HWND hwnd);
		static BOOL CALLBACK enumChildProc(HWND hwnd, LPARAM lParam);
	private:
		int mSizeXInitial, mSizeYInitial;
		int mSizeX, mSizeY;
		int mDeltaCx, mDeltaCy;
	};
/** Template Control.
	This is an interface to an operating system window that is a control within 
	a template (typically a standard control).
	*/
	class TemplateControl: public Window
	{
	public:
		TemplateControl();
		virtual ~TemplateControl();
	public:
		void attach(TemplatedUi &templatedUi, int idEntry);
		void detach();
		void setText(const Str &text);
	private:
		int mIdEntry;
	};
/** Template Pane.
	This is an interface to an operating system window that is a pane within 
	a template (typically a user defined control).
	*/
	class TemplatePane: public AttachedUi
	{
	public:
		TemplatePane();
		virtual ~TemplatePane();
	public:
		void attach(TemplatedUi &templatedUi, int idEntry);
	};
// menu frame
	class MenuFrame: public TemplatedUi
	{
	public:
		MenuFrame(uint idTemplate, uint idIcon = 0) : TemplatedUi(0, idTemplate, idIcon) {}
		~MenuFrame() {}
	protected:
		virtual void command(int command) = 0;
		virtual bool message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	};
// main menu frame
	class MainMenuFrame: public MenuFrame
	{
	public:
		MainMenuFrame(uint idTemplate, uint idIcon = 0);
		virtual ~MainMenuFrame();
	protected:
		virtual bool message(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		void quit();
	};
#endif
