// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include "types.h"
	#include "libwin.h"
// text window
	TextPane::TextPane(Window *pOwnerWindow, const Str &caption)
	: StandaloneUi(pOwnerWindow, caption), mFontSize(15), mFont(*this, mFontSize)
	{
	// initialize text
		text.insert(text.begin(), "");
	}
	TextPane::~TextPane()
	{
	}
	void TextPane::putc(int c)
	{
		if (c == '\n')
			text.insert(text.begin(), "");
		else
			*text.begin() += c;
	}
	void TextPane::paint()
	{
	// get area
		RECT size;
		::GetClientRect(*this, &size);
	// print text
		mFont.setRegion(0,0, size.right,size.bottom);
		for (int i = 0; i < text.size(); ++i)
			mFont.write(text[i], 0,i*mFontSize);
	}
// debug logging functions
	void logMessage(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		static char *messages0[] =
		{
			"null", "create", "destroy", "move", "4", "size", "activate", "setfocus",
			"killfocus", "9", "enable", "setredraw", "settext", "gettext", "gettextlength", "paint",
			"close", "queryendsession", "quit", "queryopen", "erasebkgnd", "syscolorchange", "endsession", "23",
			"showwindow", "25", "settingchange", "devmodechange", "activateapp", "fontchange", "timechange", "cancelmode",
			"setcursor", "mouseactivate", "childactivate", "queuesync", "getminmaxinfo", "37", "painticon", "iconerasebkgnd",
			"nextdlgctl", "41", "spoolerstatus", "drawitem", "measureitem", "deleteitem", "vkeytoitem", "chartoitem",
			"setfont", "getfont", "sethotkey", "gethotkey", "52", "53", "54", "querydragicon",
			"56", "compareitem", "58", "59", "60", "61", "62", "63",
			"64", "compacting", "66", "67", "commnotify", "69", "windowposchanging", "windowposchanged",
			"power", "73", "copydata", "canceljournal", "76", "77", "notify", "79",
			"inputlangchangerequest", "inputlangchange", "tcard", "help", "userchanged", "notifyformat", "86", "87",
			"88", "89", "90", "91", "92", "93", "94", "95",
			"96", "97", "98", "99", "100", "101", "102", "103",
			"104", "105", "106", "107", "108", "109", "110", "111",
			"112", "113", "114", "115", "116", "117", "118", "119",
			"120", "121", "122", "contextmenu", "stylechanging", "stylechanged", "displaychange", "geticon",
			"seticon", "nccreate", "ncdestroy", "nccalcsize", "nchittest", "ncpaint", "ncactivate", "getdlgcode",
			"syncpaint", "137", "138", "139", "140", "141", "142", "143",
			"144", "145", "146", "147", "148", "149", "150", "151",
			"152", "153", "154", "155", "156", "157", "158", "159",
			"ncmousemove", "nclbuttondown", "nclbuttonup", "nclbuttondblclk", "ncrbuttondown", "ncrbuttonup", "ncrbuttondblclk", "ncmbuttondown",
			"ncmbuttonup", "ncmbuttondblclk", "170", "171", "172", "173", "174", "175",
		};
		static char *messages1[] =
		{
			"keydown", "keyup", "char", "deadchar", "syskeydown", "syskeyup", "syschar", "sysdeadchar",
			"keylast", "265", "266", "267", "268", "269", "270", "271",
			"initdialog", "command", "syscommand", "timer", "hscroll", "vscroll", "initmenu", "initmenupopup",
			"280", "281", "282", "283", "284", "285", "286", "menuselect",
			"menuchar", "enteridle", "menurbuttonup", "291", "292", "293", "294", "295",
			"296", "297", "298", "299", "300", "301", "302", "303",
			"304", "305", "ctlcolormsgbox", "ctlcoloredit", "ctlcolorlistbox", "ctlcolorbtn", "ctlcolordlg", "ctlcolorscrollbar",
			"ctlcolorstatic", "313", "314", "315", "316", "317", "318", "319",
		};
		static char *messages2[] =
		{
			"mousemove", "lbuttondown", "lbuttonup", "lbuttondblclk", "rbuttondown", "rbuttonup", "rbuttondblclk", "mbuttondown",
			"mbuttonup", "mbuttondblclk", "mousewheel", "523", "524", "525", "526", "527",
			"parentnotify", "entermenuloop", "exitmenuloop", "nextmenu", "sizing", "capturechanged", "moving", "535",
			"powerbroadcast", "devicechange", "538", "539", "540", "541", "542", "543",
			"mdicreate", "mdidestroy", "mdiactivate", "mdirestore", "mdinext", "mdimaximize", "mdititle", "mdicascade",
			"mdiiconarrange", "mdigetactive", "554", "555", "556", "557", "558", "559",
			"mdisetmenu", "entersizemove", "exitsizemove", "dropfiles", "mdirefreshmenu", "565", "566", "567",
		};
		static char *messages3[] =
		{
			"cut", "copy", "paste", "clear", "undo", "renderformat", "renderallformats", "destroyclipboard",
			"drawclipboard", "paintclipboard", "vscrollclipboard", "sizeclipboard", "askcbformatname", "changecbchain", "hscrollclipboard", "querynewpalette",
			"paletteischanging", "palettechanged", "hotkey", "787", "788", "789", "790", "print",
			"printclient", "793", "794", "795", "796", "797", "798", "799",
		};
		switch (msg)
		{
			case WM_SETCURSOR: case WM_NCHITTEST:
			case WM_MOUSEMOVE: case WM_NCMOUSEMOVE:
			case WM_SIZING: case 867:
			case WM_ENTERIDLE:
				break;
			default:
				DebugStream::instance() << 'M' << (uint) hwnd << ':';
				if (0 <= msg && msg < 176) DebugStream::instance() << messages0[msg];
				else if (256 <= msg && msg < 320) DebugStream::instance() << messages1[msg-256];
				else if (512 <= msg && msg < 568) DebugStream::instance() << messages2[msg-512];
				else if (768 <= msg && msg < 800) DebugStream::instance() << messages3[msg-768];
				else if (1024 <= msg) DebugStream::instance() << "user+" << msg-1024;
				else DebugStream::instance() << (uint) msg;
				switch (msg)
				{
				case WM_WINDOWPOSCHANGING: case WM_WINDOWPOSCHANGED:
					{
						WINDOWPOS &windowPos = *reinterpret_cast<WINDOWPOS *>(lParam);
						DebugStream::instance() << ' ' << windowPos.x << ' ' << windowPos.y << ' ' << windowPos.cx << ' ' << windowPos.cy;
					}
					break;
				default:
					DebugStream::instance() << ' ' << wParam << ' ' << lParam;
					break;
				}
				DebugStream::instance() << std::endl;
		}
	}
