// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _TEXTWIN_H
#define _TEXTWIN_H
// text window
	class TextPane: public StandaloneUi
	{
	public:
		TextPane(Window *pOwnerWindow, const Str &caption);
		~TextPane();
		void putc(int c);
		void update() {StandaloneUi::update();}
	private:
		virtual void paint();
	private:
		int mFontSize;
		FixedFont mFont;
		std::vector<Str> text;
	};
// text window stream
	#include <ostream>
	template<class T> class WindowStreamBuffer: public std::basic_streambuf<T>
	{
	public:
		typedef typename std::char_traits<T>::int_type int_type;
		WindowStreamBuffer(TextPane &window);
		~WindowStreamBuffer();
	protected:
		virtual int sync();
		virtual int_type overflow(int_type c = std::char_traits<T>::eof());
	private:
		TextPane &mWindow;
	};
	template<class T> WindowStreamBuffer<T>::WindowStreamBuffer(TextPane &window)
	: mWindow(window)
	{
	}
	template<class T> WindowStreamBuffer<T>::~WindowStreamBuffer()
	{
	}
	template<class T> typename WindowStreamBuffer<T>::int_type WindowStreamBuffer<T>::overflow(int_type c)
	{
		mWindow.putc(c);
		return c;
	}
	template<class T> int WindowStreamBuffer<T>::sync()
	{
		mWindow.update();
		return 0;
	}
	template<class T> class WindowStream: public std::basic_ostream<T>
	{
	public:
		WindowStream(TextPane &window) : buffer(window), std::basic_ostream<T>(&buffer) {}
		~WindowStream() {}
	private:
		WindowStreamBuffer<T> buffer;
	};
// debug stream
	class DebugStream: public WindowStream<char>, public Singleton<DebugStream>
	{
	public:
		DebugStream() : WindowStream<char>(mWindow), mWindow(0, "Debug Trace") {}
		~DebugStream() {}
	private:
		TextPane mWindow;
	};
// debug logging functions
	void logMessage(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
#endif
