// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file types.h Standard Types.
	*/
#ifndef _TYPES_H
#define _TYPES_H
// include files
	#include <assert.h>
	#include <windows.h>
	#include <commctrl.h>
	#include <vector>
	#include <map>
// definitions
	#include <inttypes.h>
	#ifdef _DEBUG
		#define _CRTDBG_MAP_ALLOC
	#endif
	//#include <crtdbg.h>
// numbers
	typedef unsigned int uint;
	typedef double real;
// strings
	#include <string>
	typedef std::string Str;
// buffer template
	template<class T> class Buffer
	{
	public:
		Buffer(int size = 1) {buffer = new T[size];}
		~Buffer() {delete [] buffer;}
		operator T *() {return buffer;}
	private:
		T *buffer;
	};
// singleton template
	template<class T> class Singleton
	{
	protected:
		Singleton() {if (mPT) throw 0; mPT = static_cast<T *>(this);}
		~Singleton() {mPT = 0;}
	private:
		static T *mPT;
	public:
		inline static T &instance() {return *mPT;}
	};
	template<class T> T *Singleton<T>::mPT = 0;
#endif
