// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
	#include <sstream>
	#include <iomanip>
	#include "types.h"
	#include "libwin.h"
// chart pane
	ValuePane::ValuePane()
	{
	}
	ValuePane::~ValuePane()
	{
	}
	real &ValuePane::operator=(real r)
	{
		std::ostringstream s;
		s << std::setprecision(3) << r;
		setText(s.str());
	}
