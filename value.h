// Copyright 2002 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#ifndef _VALUE_H
#define _VALUE_H
// value pane
	class ValuePane: public TemplateControl
	{
	public:
		ValuePane();
		virtual ~ValuePane();
	public:
		real &operator=(real r);
	};
#endif
